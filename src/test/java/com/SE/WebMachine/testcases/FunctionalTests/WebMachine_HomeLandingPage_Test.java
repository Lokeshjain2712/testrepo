package com.SE.WebMachine.testcases.FunctionalTests;

import java.util.Hashtable;

import org.openqa.selenium.support.PageFactory;
import org.testng.Reporter;
import org.testng.SkipException;
import org.testng.annotations.BeforeSuite;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import com.SE.WebMachine.base.WebMachine_Base_Page;
//import com.SE.WebMachine.pages.WebMachine_DispatchPage;
import com.SE.WebMachine.pages.WebMachine_HomeLandingPage;
import com.SE.WebMachine.util.TestUtil;

public class WebMachine_HomeLandingPage_Test extends WebMachine_Base_Page{

	@BeforeSuite
	public void checkSuiteRunStatus(){
		
		//first check runmode of automation suite in test suites sheet
		if(!TestUtil.getSuiteRunmode("FunctionalTest_Suite", WebMachine_Base_Page.xls)){
			System.out.println("Skipping FunctionalTest_Suite as Runmode is N for this suite");
			throw new SkipException("Skipping FunctionalTest_Suite as Runmode is N for this suite");
		}
			//Reporter.log("Running Functional Test Suite for WebMachine", true);
		
	}
	
	@Test(dataProvider = "WebMachineHomeLandingPage_TestData")
	public void WebMachine_HomeLandingPageTest(Hashtable<String,String> data){
			
			//first check runmode of test case in test suite
			if(!TestUtil.getRunmode("WebMachine_HomeLandingPageTest", "Functional Test Cases" ,WebMachine_Base_Page.xls)){
				System.out.println("Skipping Test as Runmode is N for WebMachine_HomeLandingPageTest test case");
				throw new SkipException("Skipping Test as Runmode is N for WebMachine_HomeLandingPageTest test case");
				
			}
			// Second check runmode of each data set 
			if(data.get("Runmode").equalsIgnoreCase("N")){
				System.out.println("Skipping as Flag is N for this data set for WebMachine_HomeLandingPageTest");
				throw new SkipException("Skipping as Flag is N for this data set");
			}
			Reporter.log("Running Functional Test Suite for WebMachine", true);
			Reporter.log("Running Test Case : WebMachine_HomeLandingPageTest", true);
			fnOpenbrowser(data.get("Browser"));
			fnNavigate();
												
			WebMachine_HomeLandingPage objHomeLandingPage = PageFactory.initElements(driver, WebMachine_HomeLandingPage.class);
			
			objHomeLandingPage.fnValidate_HomeHeader();
			objHomeLandingPage.fnValidate_HomeHorizonatlNavigation();
			objHomeLandingPage.fnValidate_HomeLandingPageContent();
			objHomeLandingPage.fnValidate_EmailSignUp(data.get("EmailAddress"));
			objHomeLandingPage.fnValidate_ContactForm(data);
			
			
			}
			
	@DataProvider
	  public Object[][] WebMachineHomeLandingPage_TestData(){
	  	
	  	return TestUtil.getdata("WebMachine_HomeLandingPageTest", WebMachine_Base_Page.xls);
	  
	  }
	
	
}	
