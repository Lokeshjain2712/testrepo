/*package com.SE.WebMachine.testcases.FunctionalTests;

import java.io.OutputStream;
import java.io.PrintStream;
import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;
import java.net.URLEncoder;
import java.util.Hashtable;

import org.apache.commons.io.output.WriterOutputStream;
import org.openqa.selenium.support.PageFactory;
import org.testng.Assert;
import org.testng.Reporter;
import org.testng.SkipException;
import org.testng.annotations.BeforeSuite;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import atu.testng.reports.ATUReports;
import atu.testng.reports.logging.LogAs;
import atu.testng.selenium.reports.CaptureScreen;
import atu.testng.selenium.reports.CaptureScreen.ScreenshotOf;

import com.SE.WebMachine.base.WebMachine_Base_Page;
import com.SE.WebMachine.pages.WebMachine_DispatchPage;
import com.SE.WebMachine.util.TestUtil;

public class WebMachine_DispatchPage_Test extends WebMachine_Base_Page{

	@BeforeSuite
	public void checkSuiteRunStatus(){
		
		//first check runmode of automation suite in test suites sheet
		if(!TestUtil.getSuiteRunmode("FunctionalTest_Suite", WebMachine_Base_Page.xls)){
			System.out.println("Skipping FunctionalTest_Suite as Runmode is N for this suite");
			throw new SkipException("Skipping FunctionalTest_Suite as Runmode is N for this suite");
		}
	}
	@Test(dataProvider = "WebMachineDispatchPage_TestData")
	public void WebMachine_DispatchPageTest(Hashtable<String,String> data){
			
			//first check runmode of test case in test suite
			if(!TestUtil.getRunmode("WebMachine_DispatchPageTest", "Functional Test Cases" ,WebMachine_Base_Page.xls)){
				System.out.println("Skipping Test as Runmode is N for WebMachine_DispatchPageTest test case");
				throw new SkipException("Skipping Test as Runmode is N for WebMachine_DispatchPageTest test case");
				
			}
			// Second check runmode of each data set 
			if(data.get("Runmode").equalsIgnoreCase("N")){
				System.out.println("Skipping as Flag is N for this data set for WebMachine_DispatchPageTest");
				throw new SkipException("Skipping as Flag is N for this data set");
			}
			
			Reporter.log("Running Functional Test Suite for WebMachine", true);
			Reporter.log("Running Test Case : WebMachine_DispatchPageTest", true);
		        
			fnOpenbrowser(data.get("Browser"));
			fnNavigate();
												
			WebMachine_DispatchPage objDispatchPage = PageFactory.initElements(driver, WebMachine_DispatchPage.class);
		
			// From here write test steps and later convert them to Keywords.
					
		    objDispatchPage.fnValidate_DispatchPageContent();
		    objDispatchPage.fnValidate_Search();
			objDispatchPage.fnValidate_FooterAboutLinks();
			objDispatchPage.fnValidateFooter_LinksSection();
			objDispatchPage.fnValidate_FooterLegalsLinks();
			objDispatchPage.fnValidate_FooterSocialMedia();
			objDispatchPage.fnValidate_FooterMoreSocialOptions();
			objDispatchPage.fnValidate_HeaderSocialMedia();
			objDispatchPage.fnValidate_HeaderSocialMoreOptions();
			objDispatchPage.fnValidate_OurCompanyLink();
			objDispatchPage.fnValidate_SE_Logo();
			objDispatchPage.fnCheckCountrySelectionAjaxCall();	
			
	}
	
	
	@DataProvider
	  public Object[][] WebMachineDispatchPage_TestData(){
	  	
	  	return TestUtil.getdata("WebMachine_DispatchPageTest", WebMachine_Base_Page.xls);
	  
	  }
	
	
}
*/