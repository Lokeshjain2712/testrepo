package com.SE.WebMachine.testcases.FunctionalTests;

import java.util.Hashtable;

import org.openqa.selenium.support.PageFactory;
import org.testng.Reporter;
import org.testng.SkipException;
import org.testng.annotations.BeforeSuite;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;


import bsh.ParseException;

import com.SE.WebMachine.base.WebMachine_Base_Page;
import com.SE.WebMachine.pages.WebMachine_BeInspiredPage;
import com.SE.WebMachine.pages.WebMachine_ProductsPage;
import com.SE.WebMachine.util.TestUtil;

public class WebMachine_BeInspiredPage_Test extends WebMachine_Base_Page{
	// This will validate BeInspired landing page in @Home section.
	
	@BeforeSuite
	public void checkSuiteRunStatus(){
		
		//first check runmode of automation suite in test suites sheet
		if(!TestUtil.getSuiteRunmode("FunctionalTest_Suite", WebMachine_Base_Page.xls)){
			System.out.println("Skipping FunctionalTest_Suite as Runmode is N for this suite");
			throw new SkipException("Skipping FunctionalTest_Suite as Runmode is N for this suite");
		}else
			Reporter.log("Running Functional Test Suite for WebMachine", true);
		
	}
		
	@Test(dataProvider = "WebMachine_BeInspiredPage_TestData")
	public void WebMachine_BeInspiredPageTest(Hashtable<String,String> data){
			
			//first check runmode of test case in test suite
			if(!TestUtil.getRunmode("WebMachine_BeInspiredPageTest", "Functional Test Cases" ,WebMachine_Base_Page.xls)){
				System.out.println("Skipping Test as Runmode is N for WebMachine_BeInspiredPageTest test case");
				throw new SkipException("Skipping Test as Runmode is N for WebMachine_BeInspiredPageTest test case");
					
				}
			// Check runmode of each data set 
			if(data.get("Runmode").equalsIgnoreCase("N")){
				System.out.println("Skipping as Flag is N for this data set for WebMachine_BeInspiredPageTest");
				throw new SkipException("Skipping as Flag is N for this data set");
			}
			
	
			Reporter.log("Running Test Case : WebMachine_BeInspiredPageTest", true);
			fnOpenbrowser(data.get("Browser"));
			fnNavigate();
												
			WebMachine_BeInspiredPage objBeInspiredLandingPage = PageFactory.initElements(driver, WebMachine_BeInspiredPage.class);
			
			//objBeInspiredLandingPage.fnValidate_BeInspiredLandingPage();
			//objBeInspiredLandingPage.fnValidateDemoRoom();
			try {
				objBeInspiredLandingPage.fnValidate_GuidedSearch();
				
			} catch (Exception e) {
			System.out.println("anirudh here");
				//reporterLog("Going to click gudied search result ", true,"Going to click gudied search result ", true);
				
			} 
			
			
			
			
			}
			
	@DataProvider
	  public Object[][] WebMachine_BeInspiredPage_TestData(){
	  	
	  	return TestUtil.getdata("WebMachine_BeInspiredPageTest", WebMachine_Base_Page.xls);
	  
	  }
	
}
