package com.SE.WebMachine.testcases.FunctionalTests;

import java.util.Hashtable;

import org.openqa.selenium.support.PageFactory;
import org.testng.Reporter;
import org.testng.SkipException;
import org.testng.annotations.BeforeSuite;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import com.SE.WebMachine.base.WebMachine_Base_Page;
import com.SE.WebMachine.pages.WebMachine_ProductsPage;
import com.SE.WebMachine.pages.WebMachine_YourProjectPage;
import com.SE.WebMachine.util.TestUtil;

public class WebMachine_YourProjectPage_Test extends WebMachine_Base_Page{

	@BeforeSuite
	public void checkSuiteRunStatus(){
		
		//first check runmode of automation suite in test suites sheet
		if(!TestUtil.getSuiteRunmode("FunctionalTest_Suite", WebMachine_Base_Page.xls)){
			System.out.println("Skipping FunctionalTest_Suite as Runmode is N for this suite");
			throw new SkipException("Skipping FunctionalTest_Suite as Runmode is N for this suite");
		}
		
	}
		
	
	@Test(dataProvider = "WebMachine_YourProjectPage_TestData")
	public void WebMachine_YourProjectPageTest(Hashtable<String,String> data){
			
		//first check runmode of test case in test suite
				if(!TestUtil.getRunmode("WebMachine_YourProjectPageTest", "Functional Test Cases" ,WebMachine_Base_Page.xls)){
					System.out.println("Skipping Test as Runmode is N for WebMachine_ProductsPageTest test case");
					throw new SkipException("Skipping Test as Runmode is N for WebMachine_ProductsPageTest test case");
					
				}
			// Second check runmode of each data set 
			if(data.get("Runmode").equalsIgnoreCase("N")){
				System.out.println("Skipping as Flag is N for this data set for WebMachine_YourProjectPageTest");
				throw new SkipException("Skipping as Flag is N for this data set");
			}
			
			Reporter.log("Running Functional Test Suite for WebMachine", true);
			Reporter.log("Running Test Case : WebMachine_YourProjectPageTest", true);
			fnOpenbrowser(data.get("Browser"));
			fnNavigate();
												
			WebMachine_YourProjectPage objYourProjectLandingPage = PageFactory.initElements(driver, WebMachine_YourProjectPage.class);
			 objYourProjectLandingPage.fnValidate_YourProjectPage();
			 objYourProjectLandingPage.fnValidate_DemoProjects();
			
			
			}
			
	@DataProvider
	  public Object[][] WebMachine_YourProjectPage_TestData(){
	  	
	  	return TestUtil.getdata("WebMachine_YourProjectPageTest", WebMachine_Base_Page.xls);
	  
	  }
	
	
	
}
