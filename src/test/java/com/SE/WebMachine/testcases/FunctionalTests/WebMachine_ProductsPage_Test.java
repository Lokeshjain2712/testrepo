package com.SE.WebMachine.testcases.FunctionalTests;

import java.util.Hashtable;

import org.openqa.selenium.support.PageFactory;
import org.testng.Reporter;
import org.testng.SkipException;
import org.testng.annotations.BeforeSuite;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import com.SE.WebMachine.base.WebMachine_Base_Page;
import com.SE.WebMachine.pages.WebMachine_ProductsPage;
import com.SE.WebMachine.util.TestUtil;


public class WebMachine_ProductsPage_Test extends WebMachine_Base_Page{
	     
	@BeforeSuite
	public void checkSuiteRunStatus(){
		
		//first check runmode of automation suite in test suites sheet
		if(!TestUtil.getSuiteRunmode("FunctionalTest_Suite", WebMachine_Base_Page.xls)){
			System.out.println("Skipping FunctionalTest_Suite as Runmode is N for this suite");
			throw new SkipException("Skipping FunctionalTest_Suite as Runmode is N for this suite");
		}
		
	}
		
	
	@Test(dataProvider = "WebMachine_ProductsPage_TestData")
	public void WebMachine_ProductsPageTest(Hashtable<String,String> data){
			
		//first check runmode of test case in test suite
				if(!TestUtil.getRunmode("WebMachine_ProductsPageTest", "Functional Test Cases" ,WebMachine_Base_Page.xls)){
					System.out.println("Skipping Test as Runmode is N for WebMachine_ProductsPageTest test case");
					throw new SkipException("Skipping Test as Runmode is N for WebMachine_ProductsPageTest test case");
					
				}
			// Second check runmode of each data set 
			if(data.get("Runmode").equalsIgnoreCase("N")){
				System.out.println("Skipping as Flag is N for this data set for WebMachine_ProductsPageTest");
				throw new SkipException("Skipping as Flag is N for this data set");
			}
			
			Reporter.log("Running Functional Test Suite for WebMachine", true);
			Reporter.log("Running Test Case : WebMachine_ProductsPageTest", true);
			//ATUReports.add("Step Desc", "inputValue", "expectedValue","actualValue", false);
			fnOpenbrowser(data.get("Browser"));
			fnNavigate();
												
			WebMachine_ProductsPage objProductsLandingPage = PageFactory.initElements(driver, WebMachine_ProductsPage.class);
			 objProductsLandingPage.fnValidate_ProductsPage();
			 objProductsLandingPage.fnValidate_ProductCategory();
			
			
			}
			
	@DataProvider
	  public Object[][] WebMachine_ProductsPage_TestData(){
	  	
	  	return TestUtil.getdata("WebMachine_ProductsPageTest", WebMachine_Base_Page.xls);
	  
	  }
	
	
	
	
}
