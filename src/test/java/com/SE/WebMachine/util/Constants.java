package com.SE.WebMachine.util;

// This will have all the constants, web element locators to be used in WebMachine automation tests.

public class Constants {

	public static class Paths{
		
		// This will have Paths of all external resources.
		
		public static String  Config_FILE_PATH = System.getProperty("user.dir")+"\\src\\test\\java\\com\\SE\\WebMachine\\config\\config.properties";
		public static String XlsReader_FILE_PATH = System.getProperty("user.dir")+"\\src\\test\\java\\com\\SE\\WebMachine\\testdata\\WebMachine_TestData.xlsx";
		public static String Config_FOLDER_PATH = System.getProperty("user.dir")+"\\src\\test\\java\\com\\SE\\WebMachine\\config\\";
		public static String ATU_ReporterConfig_FILE_PATH = System.getProperty("user.dir")+"\\src\\test\\java\\com\\SE\\WebMachine\\config\\atu.properties";
	
	}
	
public class WebMachine_SearchPage{
		
		// This will have locators of the web elements on WebMachine A2 dispatch page.
		//public static final String searchInputBox = ".//*[@id='searchTerm']";  //search-field
		
		public static final String searchInputBox = ".//*[@class='search-field']"; 
		public static final String searchSubmitButton = ".//*[@id='searchForm'] //*[@value='Submit']";
		//public static final String guidedSearch = ".//*[@id='searchGuides']/span";    done by lokesh
		public static final String guidedSearch = "//*[@class='submit-search-test']";  // added by lokesh
		//public static final String searchInResultCheckbox = ".//*[@id='searchWithin']";
		public static final String searchInResultCheckbox = ".//*[@id='searchWithin-1']";
		public static final String categoryDropdownFilter = ".//select[@id='category']";
		//public static final String sortByUpdatedLink = ".//*[@id='content']/div[2]/div[2]/p/span[3]";  // done by Lokesh
		public static final String sortByUpdatedLink = ".//ul[@class='results']/li";
		public static final String searchByDateRelevanceDropdownFilter = ".//select[@id='sortBy']";
				
	}
	
	
	
	public class WebMachine_DispatchPage{
		
		// This will have locators of the web elements on WebMachine A2 dispatch page.
		
	
		
		
		//public static final String SELogoHeader_Link = "h1>a[title = 'Schneider-Electric']>img.logo-header";
		
		public static final String SelectCountry_Header_Link = "a.language-selector dropdown";
				//"//a[@class='language-selector dropdown']";
		
		public static final String OurCompnay_Header_Link = "//div[@id='header-menu']/header/nav/ul/li[2]/a";
		public static final String FaceBook_Header_Link = "//div[@class='social deployed']/a[1]";
		public static final String Twitter_Header_Link = "//div[@class='social deployed']/a[2]";
		public static final String MoreSocialMedia_Header_Link = "//div[@class='social deployed']/a[3]";
		public static final String Social_Options_Header_Link = "//ul[@class='social-options']"; // ul.social-options
		public static final String ZoneA_Link = "//a[@id='zoneA']";
		public static final String ZoneB_Link = "//a[@id='zoneB']";
		public static final String ZoneC_Link = "//a[@id='zoneC']";
		public static final String CountryName_Link = "//a[@class='language-selector dropdown']";
		public static final String ZoneSection_Link = "//div[@id='diagonal-splash']//a";
		public static final String DispatchZones = "//div[@id='diagonal-splash']";
		public static final String Continents_Links = "//ul[@class='language-details']/li";
		
		public static final String searchInputBox = ".//*[@id='searchTerm']";
		public static final String searchSubmitButton = ".//*[@id='searchForm'] //*[@value='Submit']";
		
			
	}
	
	public class WebMachine_CommonConstants{
		
		// This class will contain all the common constants(WebElements) used on different pages of WebMachine
		
		public static final String SELogoHeader_Link = "//a[@title = 'Schneider Electric']/img";
		public static final String FaceBook_Footer_Link = "//div[@class='social']/a";
		public static final String Twitter_Footer_Link = "//div[@class='social']/a[2]";
		public static final String MoreSocialMedia_Footer_Link = "//div[@class='social']/a[3]";
		public static final String Social_Options_Footer_Link = "//ul[@class='socials']";
		public static final String FooterLinks = "//ul[@id='footer-links']";      // get count of footer links, could be 2 or 3
		public static final String HomeFooter_Links = "//ul[@id='footer-links']/li[1]/ul";
		public static final String WorkFooter_Links = "//ul[@id='footer-links']/li[2]/ul";
		public static final String PartnerFooter_Links = "//ul[@id='footer-links']/li[3]/ul";
		public static final String AboutFooter_Links = "//ul[@id='footer-about']";
		public static final String SE_Legal1_Link = "//ul[@id='footer-legals']/li[1]/a";
		public static final String SE_Legal2_Link ="//ul[@id='footer-legals']/li[2]/a";
		public static final String Search_Inputbox = "//input[@id='search_input_field']";
		public static final String SearchIcon_Button = "//div[@id='search-form']/form/input[2]";
		public static final String SearchResultPage_Check = "//input[@id='search_input_field']";
		public static final String AboutUSFooterColumn_Link = "//ul[@id='footer-about']/li";
		public static final String FooterSection_Links = "//ul[@id='footer-links']/li";
		public static final String FooterLegal_Links = "//ul[@id='footer-legals']/li";
		public static final String FooterSocial_Links = "//div[@id='footer-social']/div[@class='social']/a";
		public static final String FooterMoreSocial_Link = "//div[@id='footer-social']/div/a[3]";
		public static final String FooterMoreSocialOptions_Links = "//div[@class]/ul[@id='socials']/li";
		public static final String HeaderSocial_Links = "//div[@id='header-menu']/header/div[2]/a";
		public static final String HeaderMoreSocial_Link = "//div[@id='header-menu']/header/div[2]/a[3]";
		public static final String HeaderMoreSocialOptions_Links = "//ul[@class='social-options']/li";
		public static final String CanonicalURL_Check = "//head//link[@rel='canonical']";
		public static final String LandingPageCountryName = "span.CountryTitle";
		
		
	}
	
	public class WebMachine_HomeLandingPage{
		
		public static final String LifeIsOn_Img = "//img[contains(@src,'life-is-on.png')]";
		public static final String HomeHeaderTopMenu_Links = "//nav[@class='top-menu']/ul/li";
		public static final String HomeHeaderHorizontalMenu_Links = "//div[@class='wrapper clearfix']/ul/li";
		public static final String LanguageSelectorDropdown = "//a[@class='language-selector dropdown']";
		public static final String HomeTipsSection_Links = "//ul[@class='tips-section']/li";
		public static final String HomeProductsSection_Links = "//div[@id='home-products']";
		public static final String HomePicksSection_Links = "//div[@class='picks']";
		public static final String EmailSignUp_Inputbox = "//input[@id='registrationEmailAddress']";
		public static final String EmailSignUp_Button = "//form[@id='registrationForm']/input[2]";
		public static final String EmailSubscriptionConfirmation_check = "//div[@id='thk_content']";
		public static final String ContactForm_Link = "//div[@class='wrapper clearfix']//ul/li[5]/a";
		public static final String ContactFormName_Inputbox = "//input[@id='name']";
		public static final String ContactFormCompany_Inputbox = "//input[@id='company']";
		public static final String ContactFormJobTitle_Inputbox = "//input[contains(@id,'VNPj')]";
		public static final String ContactFormEmailID_Inputbox = "//input[@id='email']";
		public static final String ContactFormPhone_Inputbox = "//input[@id='phone']";
		public static final String ContactFormAddress_Inputbox = "//textarea[contains(@id,'VNPe')]";
		public static final String ContactFormCategory_Dropbox = "//select[contains(@id,'Ap2')]";
		public static final String ContactFormDescription_Inputbox = "//textarea[@id='description']";
		public static final String ContactFormSubmit_Button = "//input[@id='submit']";
		public static final String ContactForm_Frame = "//div[@id='iframe-content']";
		public static final String ContactFormConfirmation_check = "//div[@id='contact_form_message_ok']";
		public static final String ContactFormFailMessage_check = "//div[@id='globalErrorMsgContainer']";
		
	}
	
	public class WebMachine_ProductsPage{
		
		public static final String AllProducts_Link = "div.product-list-content > h1";
		public static final String ProductsTab_Link = "//nav[@class='menu']/div/ul/li[3]/a";
		public static final String ProductsPageHeader = "div.product-list-content > h1";
		public static final String TotalProductCategories = "div.product-list-content > section";
		public static final String ProductCategoryName = "//div[@class='product-list-content']/section";
		public static final String WhereToBuy_Link = "//div[@class='desc_product']//a";
		
		
	}
	
	public class WebMachine_BeInspiredPage{
		
		public static final String BeInspiredTab_Link = "//nav[@class='menu']/div/ul/li[1]/a";
		public static final String BeInspiredPageTitle = "div#vertical_picks>h1";
		public static final String BeInspiredTiles = "//div[@id='vertical_picks']/ul[@class='tiles']/li";
		public static final String DemoRoom_Link = "//div[@id='vertical_picks']/ul/li[1]/a";
		public static final String DemoRoomPageTitle = "div#vertical_picks>h1";
		public static final String DemoRoomTiles ="//div[@id='vertical_picks']/ul[@class='tiles']/li";
		public static final String DemoRoomName = "//div[@id='vertical_picks']/ul/li[1]/a/div/span[2]/em";
		
	}
	
	public class WebMachine_YourProjectPage{
		
		public static final String YourProjectTab_Link = "//nav[@class='menu']/div/ul/li[2]/a";
		public static final String YourProjectPageTitle = "div#vertical_picks>h1>p";
		public static final String YourProjectTiles = "//div[@id='vertical_picks']/ul[@class='tiles']/li";
		public static final String YourProjectDemoTileName = "//div[@id='vertical_picks']/ul/li";
		public static final String YourProjectDemoTile_Link = "li.tall-right>a";
		public static final String YourProjectDemoTilePageTitle = "div.editorial-content>h1";
		
	}
	
}
