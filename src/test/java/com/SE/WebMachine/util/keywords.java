package com.SE.WebMachine.util;

import org.testng.AssertJUnit;

import java.awt.Robot;
import java.awt.Toolkit;
import java.awt.datatransfer.StringSelection;
import java.awt.event.KeyEvent;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileReader;
import java.io.IOException;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.net.URL;
import java.util.Calendar;
import java.util.Hashtable;
import java.util.List;
import java.util.Properties;
import java.util.concurrent.TimeUnit;

import org.testng.AssertJUnit;
import org.testng.Reporter;
import org.apache.commons.io.FileUtils;
import org.apache.log4j.Logger;
//import org.apache.System.out.println4j.BasicConfigurator;
//import org.apache.System.out.println4j.System.out.printlnger;
//import org.apache.System.out.println4j.BasicConfigurator;
//import org.apache.System.out.println4j.System.out.printlnger;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.Platform;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.ie.InternetExplorerDriver;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.remote.RemoteWebDriver;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;

import com.gargoylesoftware.htmlunit.javascript.background.JavaScriptExecutor;

// Selenium layer
/*
public class keywords {
	static Logger APPLICATION_LOGS = Logger.getLogger("devpinoyLogger");
	public WebDriver driver = null;
	Properties OR = null;    // Initialize Properties 
	Properties ENV = null;
	static keywords keyobject;
	// RemoteWebdriver
	DesiredCapabilities cap = null;
	WebDriver backup_mozilla;
	WebDriver backup_chrome;
	WebDriver backup_ie;
//	logger APPLICATION_LOGS = System.out.printlnger.getSystem.out.printlnger("devpinoySystem.out.printlnger");
	//BasicConfigurator.configure();
	@SuppressWarnings("unused")
	private keywords(){      // Constructor is made Private 
		
		OR = new Properties(); 
		ENV = new Properties();
		driver = null;
		
		try {
			
			FileInputStream fs = new FileInputStream(System.getProperty("user.dir")+"\\src\\test\\java\\com\\SE\\GSS\\config\\OR.properties");
			if(fs==null){
				System.out.println("fs is null");
			}else
			OR.load(fs);
			// Init ENV Properties
			
			String filename = OR.getProperty("environment")+".properties";
			System.out.println("Environment Filename :"+ filename);
			fs = new FileInputStream(System.getProperty("user.dir")+"\\src\\test\\java\\com\\SE\\GSS\\config\\"+filename);
			ENV.load(fs);
		} catch (Exception e) {
				//e.printStackTrace();
			 System.out.println("Unbale to load file input stream");
		} 
	}
	
	public static keywords getInstance(){
		// This will create instance of Keywords class
		
		if(keyobject==null)
			keyobject = new keywords();
		return keyobject;
	} 
	
	
	public String closeDriver(){
		// This will close driver instances
		try{
			//driver.close();
			driver.quit();
			return "Pass";
		}catch(Exception e){
			return "Fail";
		}
	}
	
	public void log(String mesg){
		APPLICATION_LOGS.debug(mesg);
	}
	public void openbrowser(String browser){
	// This will open browser
		System.out.println("Selected browser :" +browser);
		if(browser.equalsIgnoreCase("Mozilla")&& backup_mozilla!=null){
			driver = backup_mozilla;
			driver.manage().deleteAllCookies();
			return;
		}else if(browser.equalsIgnoreCase("Chrome")&& backup_chrome!=null){
			driver = backup_chrome;
			driver.manage().deleteAllCookies();
			return;
		}else if(browser.equalsIgnoreCase("IE")&& backup_ie!=null){
			driver = backup_ie;
			driver.manage().deleteAllCookies();
			return;
		}
			
	try{	
		//driver = new FirefoxDriver();
		if(browser.equalsIgnoreCase("Mozilla")){
			System.out.println("Running Scripts on Mozilla FireFox!");
			log("Running Scripts on Mozilla FireFox!");
			cap = DesiredCapabilities.firefox();
			cap.setBrowserName("Mozilla"); // chrome,iexplore
			cap.setPlatform(Platform.ANY);
			driver = new RemoteWebDriver(new URL("http://localhost:4444/wd/hub"),DesiredCapabilities.firefox());
			//driver = new FirefoxDriver();
			backup_mozilla = driver;
			
			
		}else if(browser.equalsIgnoreCase("IE")){
			System.out.println("Running Scripts on Internet Explorer!");
			log("Running Scripts on IE!");
			cap = DesiredCapabilities.internetExplorer(); // no need path of ie exe
			cap.setBrowserName("IE");
			cap.setPlatform(Platform.WINDOWS);
			driver = new RemoteWebDriver(new URL("http://localhost:4444/wd/hub"),DesiredCapabilities.internetExplorer());
			//System.setProperty("webdriver.ie.driver",System.getProperty("user.dir")+"\\drivers\\IEDriverServer.exe");
			// driver = new InternetExplorerDriver();
		//	backup_ie = driver;
			
		}else if(browser.equalsIgnoreCase("Chrome")){
			System.out.println("Running Scripts on Chrome!");
			//System.setProperty("webdriver.chrome.driver",System.getProperty("user.dir")+"\\drivers\\chromedriver.exe");
			cap = DesiredCapabilities.chrome(); // no need path of chrome exe
			cap.setBrowserName("chrome");
			cap.setPlatform(Platform.ANY);
			driver = new RemoteWebDriver(new URL("http://localhost:4444/wd/hub"),DesiredCapabilities.chrome());
			
		//	driver = new ChromeDriver();
			backup_chrome = driver;
			//driver.manage().deleteAllCookies();
		}else
			System.out.println(browser +" Browser not supported!!!");
		
		//cap.setCapability("_important", "2");
		//if(driver==null)
		//driver = new RemoteWebDriver(new URL("http://localhost:4444/wd/hub"),cap);
		
		driver.manage().deleteAllCookies();
		System.out.println("Cookies deleted...");
		driver.manage().window().maximize();
		driver.manage().timeouts().implicitlyWait(40,TimeUnit.SECONDS);
		//return "Pass";
	}catch(Throwable t){
		System.out.println("Unable to open browser : " + browser);
		ErrorUtil.addVerificationFailure(t);
		//return "Fail";
	}
	}
	
	public String navigate(){
		// This will navigate to the URL
		System.out.println("BaseURL of test application : "+ENV.getProperty("baseURL"));
		driver.get(ENV.getProperty("baseURL"));
		return "Pass";
	}
	
	public String fnSelectFilter(String strFilterValue){
	// This will select filter
		List<WebElement> liElements = null;
		System.out.println("Search_Filter value is : "+strFilterValue);
		try{
			driver.findElement(By.xpath("//span[@id='categorygroup_header_title']")).click();
			 liElements = driver.findElements(By.xpath("//div[@id='categorygroup_header_child']/ul/li"));
			 for (int i = 1; i < liElements.size()+1; i++) {
		            WebElement linkElement = driver.findElement(By.xpath("//div[@id='categorygroup_header_child']/ul/li[" + i + "]/span"));
		            System.out.println("options avaialble in dropdown : " + linkElement.getText());
		            
		            if(Search_Filter.equalsIgnoreCase("Product information")){
		        	   driver.findElement(By.xpath("//div[@id='categorygroup_header_child']/ul/li[" + i + "]")).click();
		        	   System.out.println("search filter selected!");
		           }
		        }
			if(strFilterValue.equalsIgnoreCase("Solution information")){
				driver.findElement(By.xpath("//div[@id='categorygroup_header_child']/ul/li[" + 3 + "]")).click();
				Reporter.log("Filter selected for search : " + strFilterValue, true);
			}else if(strFilterValue.equalsIgnoreCase("Product information")){
				driver.findElement(By.xpath("//div[@id='categorygroup_header_child']/ul/li[" + 2 + "]")).click();
				Reporter.log("Filter selected for search : " + strFilterValue, true);
			}else if(strFilterValue.equalsIgnoreCase("Company information")){
				driver.findElement(By.xpath("//div[@id='categorygroup_header_child']/ul/li[" + 4 + "]")).click();
				Reporter.log("Filter selected for search : " + strFilterValue, true);
			}else if(strFilterValue.equalsIgnoreCase("Services Information")){
				driver.findElement(By.xpath("//div[@id='categorygroup_header_child']/ul/li[" + 5 + "]")).click();
				Reporter.log("Filter selected for search : " + strFilterValue, true);
			}else if(strFilterValue.equalsIgnoreCase("all the site")){
				driver.findElement(By.xpath("//div[@id='categorygroup_header_child']/ul/li[" + 1 + "]")).click();
				Reporter.log("Filter selected for search : " + strFilterValue, true);
			}else{
				Reporter.log("Invalid Filter selected", true);
				Assert.fail("Invalid Filter! : " + strFilterValue);
			}
			 
			 //driver.findElement(By.xpath("//div[@id='categorygroup_header_child']/ul/li[" + 2 + "]")).click();
			 return "Pass";
		}catch(Exception e){
			
			Reporter.log("Unable to select filter! :" + strFilterValue, true);
			Assert.fail("Unable to select filter! : " + strFilterValue);
			return "Fail";
		}
		
	}
	
	
	public void fncalender(){
		
		WebElement dateWidget = driver.findElement(By.xpath("//table[@class='jCalendar']/tbody"));  
		  List<WebElement> rows=dateWidget.findElements(By.tagName("tr"));  
		  List<WebElement> columns=dateWidget.findElements(By.tagName("td"));  
		    
		  for (WebElement cell: columns){  
		   //Select 13th Date   
		   if (cell.getText().equals("13")){  
		   cell.findElement(By.linkText("13")).click();  
		   break;  
		   }  
		  }   
		
		
	}
	public String validate_GuidedSearch(String keywordsearched){
		// This will validate if options are available once first 3 letters are entered in searchbox as guided search.
		List<WebElement> liElements = null;
		try{
	    liElements = driver.findElements(By.xpath("//ul[@class='ui-autocomplete ui-menu ui-widget ui-widget-content']/li"));

	 	Reporter.log("Total options available as guided search for keyword :" +  keywordsearched   + " are : " + liElements.size(), true);
	 	for (int i = 1; i < liElements.size()+1; i++) {
            WebElement linkElement = driver.findElement(By.xpath("//ul[@class='ui-autocomplete ui-menu ui-widget ui-widget-content']/li[" + i + "]/a"));
            System.out.println(linkElement.getText());
           
        }
	 	driver.findElement(By.xpath("//ul[@class='ui-autocomplete ui-menu ui-widget ui-widget-content']/li[" + 1 + "]/a")).click();
	 	fnValidateGlobalSiteSearchPage(keywordsearched);
	 	 return "Pass";
		}catch(Exception e){
			Reporter.log("Guided search not available for keyword : " + keywordsearched, true);
			Assert.fail("Guided search not available for keyword : " + keywordsearched);
			return "Fail";
		}
		
      		
	}
	public void fnValidateGlobalSiteSearchPage(String keywordsearched){
		String strCurrentTitle = driver.getTitle();
		String strExpectedTitle = "Global Site Search";
		if(strCurrentTitle.contains(strExpectedTitle)){
			Reporter.log("Global Site Search page is avaiable for keywordsearched : " + keywordsearched, true);
		}else{
			Reporter.log("Global Site Search page is NOT avaiable for keywordsearched : " + keywordsearched, true);
			Assert.fail("Global Site Search page is NOT avaiable for keywordsearched : " + keywordsearched);
		}
		
	}
	public String validateLoginErrorMesg(String UserID, String Password){
		System.out.println("Validating error mesgs for invalid login attempts");
		String flag = null;
		if(UserID.isEmpty()||Password.isEmpty()){
			if(UserID.isEmpty()&& !Password.isEmpty()){
				try{
					WebElement element = driver.findElement(By.xpath("//button[contains(@class,'btn-primary') and not(@disabled)]"));
				    JavascriptExecutor executor = (JavascriptExecutor)driver;
				    executor.executeScript("arguments[0].click();", element);
				    String error_msg = driver.findElement(By.xpath("//div[contains(@class,'required') and contains(@role,'alert')]")).getText();
					
					if(!error_msg.isEmpty()){
						System.out.println("UserID field is Empty/Null, Hence login failed with Password : " + Password);
						System.out.println("User login failed, Error mesg displayed :  " + error_msg);
						flag = "Pass";
					}else{
						flag = "Fail";
						AssertJUnit.fail("User login failed, Error mesg not displayed :  " + error_msg);
					}
				}catch(Throwable t){
					
					System.out.println("Error ! This Field is required message not displayed for Empty UserID");
					//validatelogin(UserID, Password);
					ErrorUtil.addVerificationFailure(t);
					flag = "Fail";
				}
				
			}
			if(Password.isEmpty()&& !UserID.isEmpty()){
				try{
					WebElement element = driver.findElement(By.xpath("//button[contains(@class,'btn-primary') and not(@disabled)]"));
				    JavascriptExecutor executor = (JavascriptExecutor)driver;
				    executor.executeScript("arguments[0].click();", element);
				    String error_msg = driver.findElement(By.xpath("//div[contains(@class,'required') and contains(@role,'alert')]")).getText();
					
					if(!error_msg.isEmpty()){
						System.out.println("Password field is Empty/Null, Hence login failed with UserID : " + UserID);
						System.out.println("User login failed, Error mesg displayed :  " + error_msg);
						flag = "Pass";
					}else{
						flag = "Fail";
						System.out.println("Error ! This Field is required message not displayed for Empty Password");
					}
				}catch(Exception e){
					
					System.out.println("Error ! This Field is required message not displayed for Empty Password");
					//validatelogin(UserID, Password);
					flag = "Fail";
				}
			}
			if(UserID.isEmpty()&&Password.isEmpty()){
				System.out.println("User ID and Password are both Empty/Null");
		try{
				WebElement element = driver.findElement(By.xpath("//button[contains(@class,'btn-primary') and not(@disabled)]"));
			    JavascriptExecutor executor = (JavascriptExecutor)driver;
			    executor.executeScript("arguments[0].click();", element);
			    String error_msg = driver.findElement(By.xpath("//div[contains(@class,'required') and contains(@role,'alert')]")).getText();
				if(!error_msg.isEmpty()){
					System.out.println("Error message displayed for Empty UserID and Password is available as :  "+ error_msg);
				}else
					System.out.println("Error message NOT displayed for Empty UserID and Password");
			}
			catch(Exception e){
				System.out.println("Error message NOT displayed for Empty UserID and Password");
			}
			}
		}
	
	else{
		try{
			boolean bol = driver.findElement(By.xpath(OR.getProperty("AuthFailedMesg_Text"))).isDisplayed();
			String error_mesg = driver.findElement(By.xpath(OR.getProperty("AuthFailedMesg_Text"))).getText();
			if(bol){
				System.out.println("UserID :  " + UserID + "   Password :  " + Password);
				System.out.println("User login failed, Error mesg displayed :  " + error_mesg);
				
				flag = "Pass";
			}else{
				System.out.println("Error mesg NOT displayed ");
				
			}
				
		}catch(Exception e){
			
			String result = validatelogin(UserID, Password);
			if(result.equalsIgnoreCase("Pass")){
				Reporter.log("Error Mesg NOT displayed because Login successfull with LoginID " + UserID + " and Password " + Password, true);
				flag = "Fail";
				Assert.fail("Error Mesg NOT displayed becoz Login successfull with LoginID " + UserID + " and Password " + Password);
				
			}
			
		}
		return flag;
		}
		return flag;
	}

	
	public String validatelogin(String UserID, String Password){
		// This will validate user login if successful or not
		//WebDriverWait wait = new WebDriverWait(driver,10);
		try {
			Thread.sleep(1000);
		} catch (InterruptedException e) {
			
			e.printStackTrace();
		}
		String landingPageUrl = driver.getCurrentUrl();
		System.out.println("landingPageUrl is :  " +landingPageUrl);
		boolean bol1 = landingPageUrl.contains(UserID);
		boolean bol2 = landingPageUrl.contains(Password);
		if(bol1==true){
			System.out.println("Landing page Url contains UserID : " + UserID);
		}else if(bol2==true){
			System.out.println("Landing page Url contains Password : " + Password);
		}else {
			Reporter.log("Landing page URL doesn't contains UserID or Password", true);
		}
		try{
			String title = driver.getTitle();
			System.out.println("Title of page : " + title + "\n");
			
			Reporter.log("Tittle of the page :  " + title);
			
			//AssertJUnit.assertEquals("Dashboard - Liferay", driver.getTitle());
			Assert.assertEquals("Dashboard - Liferay", title);
			Reporter.log("Login Successfull! with UserID : " + UserID + "  and Password : " + Password, true);
			return "Pass";
			
		}catch(Throwable t){
			ErrorUtil.addVerificationFailure(t);
			Reporter.log("Login Failed with UserID : " + UserID + "  and Password : " + Password, true);
			Reporter.log("Steps to reproduce the failure : ");
			Reporter.log("Step 1 > Open Browser ");
			Reporter.log("Step 2 > Enter UserId in Screen name field as : " + UserID);
			Reporter.log("Step 3 > Enter Password in password field as : " + Password);
			Reporter.log("Link for Screenshot of failing screen : " );
			AssertJUnit.fail("Login Failed with UserID : " + UserID + "  and Password : " + Password);
			
			return "Fail ";
		}
	}
	public String validateRememberMe(String xpathkey, String UserID){
		// This will check if UserID is available in Screen name input box when remember me checked.
		//System.out.println(xpathkey + "    " + UserID);
		try{
			String userIDAvailable = driver.findElement(By.xpath(OR.getProperty(xpathkey))).getAttribute("value");
			System.out.println("userIDAvailable : " + userIDAvailable);
			Assert.assertEquals(UserID, userIDAvailable);
			Reporter.log("UserID is available in Screen name input box as : " + userIDAvailable +" is same as used for login : " + UserID , true);
				return "Pass";
				
				
			
		}catch(Throwable t){
			ErrorUtil.addVerificationFailure(t);
			Reporter.log("Error! UserID is Not available in Screen name input box which was used for login : " + UserID , true);
			Assert.fail("Error! UserID is Not available in Screen name input box which was used for login : " + UserID);
			return "Fail";
		}
		
	}
	public String logout(){
		
		//System.out.println("going to logout now...");
				WebElement element = driver.findElement(By.cssSelector("span.user-full-name"));
				 JavascriptExecutor executor = (JavascriptExecutor)driver;
			    executor.executeScript("arguments[0].click();", element);
			    
			    element = driver.findElement(By.xpath("//a[contains(@href, '/c/portal/logout')]"));
			    executor.executeScript("arguments[0].click();", element);
			    return "pass";
		try{
			
			driver.findElement(By.cssSelector("span.user-full-name")).click();
			driver.findElement(By.xpath("//a[contains(@href, '/c/portal/logout')]")).click();
			System.out.println("Logout done");
			
			return "Pass";
		}catch(Exception e){
			return "Fail";
		}
		
	}
	
	public String click(String xpathkey){
		// This will click
		try {
			
			driver.findElement(By.xpath(OR.getProperty(xpathkey))).click();
			System.out.println("Click done on " + xpathkey);
			return "Pass";
		}catch(Exception e){
			System.out.println("Not found :" + xpathkey);
			return "Fail";
		}
		
	}
	public static void setClipboardData(String string) {
		   StringSelection stringSelection = new StringSelection(string);
		   Toolkit.getDefaultToolkit().getSystemClipboard().setContents(stringSelection, null);
		}
	// File Upload Function
	public void fnFileUpload(){
		
		try{
			setClipboardData("C:\\testfile\\uploadfile.xlsx");
			Robot robot = new Robot(); 
			robot.delay(1000);
			robot.keyPress(KeyEvent.VK_CONTROL);
			robot.keyPress(KeyEvent.VK_V);
			robot.keyRelease(KeyEvent.VK_V);
			robot.keyRelease(KeyEvent.VK_CONTROL);
			robot.keyPress(KeyEvent.VK_ENTER);
			robot.keyRelease(KeyEvent.VK_ENTER);

			robot.delay(1000);
			System.out.println("File upload done");
		}catch(Exception e){
			System.out.println("Unable to upload file");
			Assert.fail("File upload failed");
		}
		
		
		
		try{
		WebElement fileInput = driver.findElement(By.xpath("//input[@type='file']"));
		fileInput.sendKeys("C:/testfile/uploadfile.xls");
		System.out.println("File upload done");
		}catch(Exception e){
			System.out.println("Unable to upload file");
			Assert.fail("File upload failed");
		}
	}
	
	
	
	
	
	
	
	
	
	
	public String validateUrl(){
		String flag;
		String actualUrl = driver.getCurrentUrl();
		String currenturl[] = actualUrl.split("/");
		String expectedUrl = ENV.getProperty("Partner_StageURL");
		int result = currenturl[2].compareToIgnoreCase(expectedUrl);
	    if(result==0)
	    {
	    System.out.println("You are on isel site of "+ OR.getProperty("environment") + " environment ");
	    flag = "Pass";
	    return flag;
	    }else{
	    	System.out.println("Error! You are NOT on isel site of "+ OR.getProperty("environment") + " environment ");
	    	
	    	try{
	    	AssertJUnit.fail("Error! You are NOT on isel site of "+ OR.getProperty("environment") + " environment ");
	    	}catch(Throwable t){
	    		// exception caught
	    	}
	    	System.out.println("Hence Exiting test now!!!");
	    	flag = "Fail";
	    	driver.close();
	    	return flag;
	}
	
	
	}
	
	
	public String input(String xpathkey, String inputtext){
		// This will handle text input fields
		try{
			driver.findElement(By.xpath(OR.getProperty(xpathkey))).clear();
			driver.findElement(By.xpath(OR.getProperty(xpathkey))).sendKeys(inputtext);
			return "Pass";
		}catch(Exception e){
			System.out.println("Input box for entering "+ xpathkey +"not found!");
			e.printStackTrace();
			return "Fail";
		}
		
	}
public String dropdownMenu(String xpathkey, String valueToSelect){
		
		System.out.println("Vaule to be selected in dropdownMenu : "+ valueToSelect);
		try{
			String alloptions = driver.findElement(By.xpath(OR.getProperty(xpathkey))).getText();
			System.out.println("All options in dropbox : " + alloptions);
			Select dropbox = new Select(driver.findElement(By.xpath(OR.getProperty(xpathkey))));
			dropbox.selectByVisibleText(valueToSelect);
			System.out.println("Vaule selected from dropdown menu :" +valueToSelect );
			return "Pass";

		   }catch (Throwable t){
			System.out.println("Error! Value "+valueToSelect+" is not available in dropdown options");
			ErrorUtil.addVerificationFailure(t);
			return "Fail";
		}
	}

	
	// This will read/execute keywords from Test Steps sheet.
	public void executeKeywords(String testName, Xls_Reader xls, Hashtable<String, String> table){
		
		int rows = xls.getRowCount("Test Steps");
		for (int rNum=2; rNum<=rows; rNum++){
			String tcid = xls.getCellData("Test Steps", "TCID", rNum);
			if(tcid.equalsIgnoreCase(testName)){  
				// This if will check if the test case name passed is available in TCID column
		
				String keyword = xls.getCellData("Test Steps", "Keyword", rNum);
				String object = xls.getCellData("Test Steps", "Object", rNum);
				String data = xls.getCellData("Test Steps", "Data", rNum);
				String result = "";
				String featureFlip_Flag = xls.getCellData("Test Steps", "Feature_Flip", rNum);
				System.out.println("value in feature flip flag : " + featureFlip_Flag);
				// execute keywords
			if(featureFlip_Flag.equalsIgnoreCase("ON")){
				switch(keyword)
				{
				case "openbrowser" :{
					result = "Pass";
					openbrowser(table.get(data));
					System.out.println("Opening browser : " + table.get(data));
					break;
				}
				case "navigate" :{
					result = navigate();
					break;
				}	
				
				case "click" :{
					result = click(object);
					break;
				}
				case "input" :{
					result = input(object,table.get(data));
					break;
				}
				case "validatelogin" :{
					String dataarr[] = data.split(",");
					log("At index [0] "+dataarr[0]+ " at index[1] "+ dataarr[1]);
					result = validatelogin(table.get(dataarr[0]),table.get(dataarr[1]));
					break;
				}
				case "validateLoginErrorMesg" :{
					String dataarr[] = data.split(",");
					log("At index [0] "+dataarr[0]+ " at index[1] "+ dataarr[1]);
					result = validateLoginErrorMesg(table.get(dataarr[0]),table.get(dataarr[1]));
					break;
				}
				case "validateRememberMe" :{
					result = validateRememberMe(object, table.get(data));
					break;
				}
				case "logout" : {
					result = logout();
					break;
				}
			
				default :
					break;
				}
					
												
				// This will check if continue or stop test run on test step failure
				if(result.equalsIgnoreCase("Pass")){
					if(table.get(data)!=null){
					System.out.println(tcid+"-------"+keyword+"---------"+object+"-------"+data+": "+ table.get(data) +"------"+ result);
					}else{
						System.out.println(tcid+"-------"+keyword+"---------"+object+"-------"+data +"------"+ result);
					}
					
				}else{
					if(table.get(data)!=null){
						System.out.println("Error"+ tcid+"-------"+keyword+"---------"+object+"-------"+data+ ": "+ table.get(data) +"------"+ result);
						}else{
							System.out.println("Error! "+tcid+"-------"+keyword+"---------"+object+"-------"+data +"------"+ result);
						}
					
				}
					
				if(!result.equalsIgnoreCase("Pass")){
					
					// This will take screenshot of the error on page.
					try{
						String fileName = tcid+"_"+keyword+rNum+".jpg";
						File srcFile = ((TakesScreenshot)driver).getScreenshotAs(OutputType.FILE);
						FileUtils.copyFile(srcFile, new File(System.getProperty("user.dir")+"//Screenshots//"+fileName));
						System.out.println("Screenshot taken of the failing screen");
					}catch(Throwable t){
						System.out.println("Unable to take Screenshot of the failing screen");
						ErrorUtil.addVerificationFailure(t);
					}
					String proceed = xls.getCellData("Test Steps", "Proceed_on_Fail", rNum);
					if(proceed.equalsIgnoreCase("Y")){
						// report fail and continue with the test
						try{
							AssertJUnit.fail(tcid+"-------"+keyword+"---------"+object+"-------"+data+"------"+ result);
						}catch(Throwable t){
							ErrorUtil.addVerificationFailure(t);
							//System.out.println("**********Error********");
						}
					}else
					{
						// report fail and stop test
						System.out.println("Error! Stoping test now"+ tcid+"-------"+keyword+"---------"+object+"-------"+data+"------"+ result);
						AssertJUnit.fail(tcid+"-------"+keyword+"---------"+object+"-------"+data+"------"+ result);
					}
				}
			}else{
				
				System.out.println("Feature flip flag is set to OFF position, hence skipped");
			}
				
			
		}
		
	}
	}
	
	}
*/