package com.SE.WebMachine.util;

import java.net.Socket;

import org.openqa.selenium.server.RemoteControlConfiguration;
import org.openqa.selenium.server.SeleniumServer;
import org.testng.annotations.AfterSuite;
import org.testng.annotations.BeforeSuite;
import org.testng.annotations.Test;

import com.thoughtworks.selenium.SeleneseTestNgHelper;

/**
 * The goal of this class is to prepare the test suite execution. In this class,
 * we will only start a local selenium server and stop it once the test suite is
 * finished.
 * 
 * Before we start the selenium server, we need to check if no instance is
 * running already. To achieve that, we'll assume all selenium servers are
 * running on port #4444 and we'll test if the port is open or not.
 * 
 * To enable selenium server, this class should be added as a test in your test
 * suite. prepareSuite() and tearDownSuite() methods will be automatically
 * invoked thanks to annotations.
 * 
 * @author Fabien
 * 
 */
public class SeleniumSetup extends SeleneseTestNgHelper {
	private SeleniumServer server = null;
	private static boolean stopServer = true;

	@BeforeSuite
	public void prepareSuite() {

		// test if there is a local selenium server running
		try {
			System.out
					.println("Testing local selenium server --------------------");
			Socket ServerSok = new Socket("127.0.0.1", 4444);
			System.out.println("   One selenium server running on port# 4444");
			// if a server is running, we want to make sure we don't stop it at the end of the test suite
			ServerSok.close();
			SeleniumSetup.setStopServer(false);
			return;

		} catch (Exception e) {
			// e.printStackTrace();
			System.out.println("   No selenium server running on port# 4444");

		}

		RemoteControlConfiguration rcc = new RemoteControlConfiguration();

		// start a local instance of selenium server
		try {
			System.out
					.println("Starting local selenium server --------------------");
			server = new SeleniumServer(rcc);
			server.start();
			System.out
					.println("Local selenium server started ---------------------");
		} catch (Exception e) {
			throw new IllegalStateException("Can't start selenium server", e);
		}
	}

/*
	@AfterSuite
	public void tearDownSuite() {
		if (!SeleniumSetup.isStopServer()) {
			return;
		}
		System.out.println("Stoping local selenium server ---------------------");
		//server.stop();
	}
*/
	public static boolean isStopServer() {
		return stopServer;
	}

	public static void setStopServer(boolean stopServer) {
		SeleniumSetup.stopServer = stopServer;
	}

}
