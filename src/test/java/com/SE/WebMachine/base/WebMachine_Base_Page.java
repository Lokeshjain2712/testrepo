package com.SE.WebMachine.base;
import java.io.FileInputStream;
import java.io.PrintStream;
import java.io.UnsupportedEncodingException;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;
import java.util.Properties;
import java.util.concurrent.TimeUnit;

import org.apache.log4j.Logger;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.Platform;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.remote.RemoteWebDriver;
import org.openqa.selenium.support.FindAll;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.ui.ExpectedCondition;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;
import org.testng.Reporter;

import java.util.Arrays;
import org.openqa.selenium.Proxy;
import org.openqa.selenium.support.ui.ExpectedCondition;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;

import com.SE.WebMachine.util.Constants;
import com.SE.WebMachine.util.ErrorUtil;
import com.SE.WebMachine.util.Xls_Reader;

public class WebMachine_Base_Page{
	static Logger APPLICATION_LOGS = Logger.getLogger("devpinoyLogger");
	 //Set Property for ATU Reporter Configuration
    {
      //System.setProperty("atu.reporter.config", "C:\\Users\\SESA296884\\WebMachine_Workspace\\WebMachine_Aut_Framework\\src\\test\\java\\com\\SE\\WebMachine\\config\\atu.properties");
    	System.setProperty("atu.reporter.config", "D:\\krishanWS\\WebMachine_Aut_Framework\\src\\test\\java\\com\\SE\\WebMachine\\config\\atu.properties");

    }
	public static WebDriver driver = null;
	protected Properties Config = null;    // Initialize Properties 
	protected Properties ENV = null;
	static WebMachine_Base_Page objBase;
	public static Xls_Reader xls = new Xls_Reader(Constants.Paths.XlsReader_FILE_PATH);
	DesiredCapabilities cap = null;
	static WebDriver backup_mozilla;
	static WebDriver backup_chrome;
	static WebDriver backup_ie;
//	logger APPLICATION_LOGS = System.out.printlnger.getSystem.out.printlnger("devpinoySystem.out.printlnger");
	//BasicConfigurator.configure();
	
	@FindBy(xpath = Constants.WebMachine_CommonConstants.FaceBook_Footer_Link)
	public WebElement FaceBook_Footer_Link;
	
	@FindBy(xpath = Constants.WebMachine_CommonConstants.Twitter_Footer_Link)
	public WebElement Twitter_Footer_Link;
	
	@FindBy(xpath = Constants.WebMachine_CommonConstants.MoreSocialMedia_Footer_Link)
	public WebElement MoreSocialMedia_Footer_Link;
	
	@FindBy(xpath = Constants.WebMachine_CommonConstants.Social_Options_Footer_Link)
	public WebElement Social_Options_Footer_Link;
	
	@FindBy(xpath = Constants.WebMachine_CommonConstants.SE_Legal1_Link)
	public WebElement SE_Legal1_Link;
	
	@FindBy(xpath = Constants.WebMachine_CommonConstants.SE_Legal2_Link)
	public WebElement SE_Legal2_Link;
	
	@FindBy(xpath = Constants.WebMachine_CommonConstants.AboutFooter_Links)
	public WebElement AboutFooter_Links;
	
	@FindBy(xpath = Constants.WebMachine_CommonConstants.HomeFooter_Links)
	public WebElement HomeFooter_Links;
	
	@FindBy(xpath = Constants.WebMachine_CommonConstants.WorkFooter_Links)
	public WebElement WorkFooter_Links;
	
	@FindBy(xpath = Constants.WebMachine_CommonConstants.PartnerFooter_Links)
	public WebElement PartnerFooter_Links;
	
	@FindBy(xpath = Constants.WebMachine_CommonConstants.Search_Inputbox)
	public WebElement Search_Inputbox;
	
	@FindBy(xpath = Constants.WebMachine_CommonConstants.SearchIcon_Button)
	public WebElement SearchIcon_Button;
	
	@FindBy(xpath = Constants.WebMachine_CommonConstants.SearchResultPage_Check)
	public WebElement SearchResultPage_Check;
	
	@FindBy(how = How.XPATH, using = Constants.WebMachine_CommonConstants.FooterSection_Links)
	public WebElement FooterSection_Links;
	
	@FindBy(how = How.XPATH, using = Constants.WebMachine_CommonConstants.FooterLegal_Links)
	public WebElement FooterLegal_Links;
	
	@FindBy(how = How.XPATH, using = Constants.WebMachine_CommonConstants.FooterSocial_Links)
	public WebElement FooterSocial_Links;
	
	@FindBy(how = How.XPATH, using = Constants.WebMachine_CommonConstants.FooterMoreSocial_Link)
	public WebElement FooterMoreSocial_Link;
	
	@FindBy(how = How.XPATH, using = Constants.WebMachine_CommonConstants.FooterMoreSocialOptions_Links)
	public WebElement FooterMoreSocialOptions_Links;
	
	@FindBy(how = How.XPATH, using = Constants.WebMachine_CommonConstants.HeaderSocial_Links)
	public WebElement HeaderSocial_Links;
	
	@FindBy(how = How.XPATH, using = Constants.WebMachine_CommonConstants.HeaderMoreSocialOptions_Links)
	public WebElement HeaderMoreSocialOptions_Links;
	
	@FindBy(how = How.XPATH, using = Constants.WebMachine_CommonConstants.HeaderMoreSocial_Link)
	public WebElement HeaderMoreSocial_Link;
	
	@FindBy(how = How.XPATH, using = Constants.WebMachine_CommonConstants.SELogoHeader_Link)
	public WebElement SELogoHeader_Link;
	
	@FindBy(how = How.XPATH, using = Constants.WebMachine_DispatchPage.ZoneA_Link)
	public WebElement ZoneA_Link;
	
	@FindBy(how = How.XPATH, using = Constants.WebMachine_DispatchPage.CountryName_Link)
	public WebElement CountryName_Link;
	
	@FindBy(how = How.XPATH, using = Constants.WebMachine_CommonConstants.CanonicalURL_Check)
	public WebElement CanonicalURL_Check;
	
	@FindBy(how = How.CSS, using = Constants.WebMachine_DispatchPage.SelectCountry_Header_Link)
	public WebElement SelectCountry_Header_Link;
	
	@FindAll({@FindBy(how = How.XPATH, using = Constants.WebMachine_HomeLandingPage.HomeHeaderTopMenu_Links)})
	public List<WebElement> HomeHeaderTopMenu_Links;
	
	@FindAll({@FindBy(how = How.XPATH, using = Constants.WebMachine_HomeLandingPage.HomeHeaderHorizontalMenu_Links)})
	public List<WebElement> HomeHeaderHorizontalMenu_Links;
	
	@FindAll({@FindBy(how = How.XPATH, using = Constants.WebMachine_DispatchPage.Continents_Links)})
	public List<WebElement> Continents_Links;
	
	@FindBy(how = How.CSS, using = Constants.WebMachine_CommonConstants.LandingPageCountryName)
	public WebElement LandingPageCountryName;
	
	
	/* Code added by Krishan starts here */
	
	public static boolean isElementExist(String elementXpath, String AssertErrorMessage){	
		
		Reporter.log("checking if "+driver.findElement(By.xpath(elementXpath)).getTagName()+ "  exist?",true);
		Assert.assertTrue(isElementAvailable(elementXpath), AssertErrorMessage);
		if(isElementAvailable(elementXpath))
		{
		Reporter.log(driver.findElement(By.xpath(elementXpath)).getTagName()+ "  exists",true);
		}
		return isElementAvailable(elementXpath);
		
  }
	public static boolean isElementAvailable(String elementXpath){
		//Reporter.log(driver.findElement(By.xpath(elementXpath)).getTagName()+ "  exists",true);
		return driver.findElements(By.xpath(elementXpath)).size()!=0;
	}	
	
	public static void Click(String elementXpath, String AssertErrorMessage){
		if(isElementExist(elementXpath, AssertErrorMessage)){
			driver.findElement(By.xpath(elementXpath)).click();
			Reporter.log("click done",true);
		}
	}
	
	public static void ClickAtuLog(String elementXpath, String AssertErrorMessage, String reporterLogMessage, boolean reporterFlag, String AtuReportMessage, Boolean AtuLogStatus){
		System.out.println("ClickAtuLog called");
		try{
		if(isElementExist(elementXpath, AssertErrorMessage)){
			driver.findElement(By.xpath(elementXpath)).click();
			//reporterLog(reporterLogMessage, reporterFlag, AtuReportMessage, AtuLogStatus);
			//repLog(reporterLogMessage, reporterFlag, AtuReportMessage);
		}
		}catch(Throwable t){
			System.out.println("Inside catch of clcikatulog");			
			reporterLog("Error! in ClickAtuLog ", true, "Error! @Home ClickAtuLog failed", true);		
			//ErrorUtil.addVerificationFailure(t);
	}
	}
	
	public static void reporterLog(String reporterLogMessage, boolean reporterFlag, String AtuReportMessage, Boolean AtuLogStatus){
		Reporter.log(reporterLogMessage , reporterFlag);		
		//////ATUReports.add(AtuReportMessage, AtuLogStatus, new CaptureScreen(ScreenshotOf.BROWSER_PAGE));		
	}
	
	public static void repLog(String reporterLogMessage, boolean reporterFlag, String AtuReportMessage){
		Reporter.log(reporterLogMessage , reporterFlag);		
		////////ATUReports.add(AtuReportMessage, LogAs.PASSED, new CaptureScreen(ScreenshotOf.BROWSER_PAGE));
		System.out.println("reporterLog done");
	}
	
	public static void reporterLogOnly(String reporterLogMessage, boolean reporterFlag){
		Reporter.log(reporterLogMessage , reporterFlag);	
		
	} 
	
	public static <WebElements> List<WebElements> getWebElementsAllIsExist(String elementXpath, String AssertErrorMessage){
		if(isElementExist(elementXpath, AssertErrorMessage));
		List<WebElements> WebElems = null;
		if(isElementAvailable(elementXpath)){
		Reporter.log("getting list of links",true);
			WebElems = (List<WebElements>) driver.findElements(By.xpath(elementXpath));
			Reporter.log("got size of list of links " + WebElems.size(),true);
			return  WebElems;
		}		
		return null;
		
	}
	
	public static void getWebElementsAllIsExistClick(String elementXpath, int elementIndex, String AssertErrorMessage, String reporterLogMessage, boolean reporterFlag, String AtuReportMessage, Boolean AtuLogStatus){
		if(isElementExist(elementXpath, AssertErrorMessage));
		List<WebElement> WebElems = null;
		if(isElementAvailable(elementXpath)){
			WebElems = driver.findElements(By.xpath(elementXpath));
			WebElems.get(elementIndex).click();
			//reporterLog(reporterLogMessage, reporterFlag, AtuReportMessage, AtuLogStatus);
		}		
	}
	
	
	public static void sendKeys(String elementXpath, String value, String AssertErrorMessage){
		if(isElementExist(elementXpath, AssertErrorMessage)){
				driver.findElement(By.xpath(elementXpath)).sendKeys(value);
			}		
	}
	
	public static void sendKeysAtuLog(String elementXpath, String value, String AssertErrorMessage, String reporterLogMessage, boolean reporterFlag, String AtuReportMessage, Boolean AtuLogStatus){
		if(isElementExist(elementXpath, AssertErrorMessage)){
				driver.findElement(By.xpath(elementXpath)).sendKeys(value);
				reporterLog(reporterLogMessage, reporterFlag, AtuReportMessage, true);
			}		
		}
	
	public static void selectDropdownElementByIndex(String elementXpath, int index, String AssertErrorMessage){
		if(isElementExist(elementXpath, AssertErrorMessage)){
			Select sel = new Select(driver.findElement(By.xpath(elementXpath)));
			sel.selectByIndex(index);			
		}	
	}
	
	public static void selectDropdownElementByIndexAtuLog(String elementXpath, int index, String AssertErrorMessage, String reporterLogMessage, boolean reporterFlag, String AtuReportMessage, Boolean AtuLogStatus){
		if(isElementExist(elementXpath, AssertErrorMessage)){
			Select sel = new Select(driver.findElement(By.xpath(elementXpath)));
			sel.selectByIndex(index);
			reporterLog(reporterLogMessage, reporterFlag, AtuReportMessage, true);
		}	
	}	
	
	public static void selectDropdownElementByVisibleText(String elementXpath, String visibleText, String AssertErrorMessage){
		if(isElementExist(elementXpath, AssertErrorMessage)){
			Select sel = new Select(driver.findElement(By.xpath(elementXpath)));
			Reporter.log("Before selecting the Visible text",true);
			sel.selectByValue(visibleText);
			//sel.selectByVisibleText(visibleText);	
			Reporter.log("After selecting the Visible text",true);
			reporterLogOnly(visibleText +" selected from dropdown" , true);
		}	
	}
	
	public static void selectDropdownElementByVisibleTextAtuLog(String elementXpath, String visibleText, String AssertErrorMessage, String reporterLogMessage, boolean reporterFlag, String AtuReportMessage, Boolean AtuLogStatus){
		if(isElementExist(elementXpath, AssertErrorMessage)){
			Select sel = new Select(driver.findElement(By.xpath(elementXpath)));
			sel.selectByVisibleText(visibleText);
			reporterLog(reporterLogMessage, reporterFlag, AtuReportMessage, true);
		}	
	}
	
	public static String getByAttribute(String elementXpath, String byAttribute, String AssertErrorMessage, String reporterLogMessage, boolean reporterFlag, String AtuReportMessage, Boolean AtuLogStatus){
		String attResponse = "";
		if(isElementAvailable(elementXpath)){
			attResponse = driver.findElement(By.xpath(elementXpath)).getAttribute(byAttribute);
			reporterLog(reporterLogMessage, reporterFlag, AtuReportMessage, true);
		}	
		return attResponse;
	}
	
	/*
	public static Boolean getLogAs(String objLogAs) {
        LogAs logAsStatus = LogAs.PASSED;               
        switch (objLogAs) {         
        case "PASSED":
        	logAsStatus = LogAs.PASSED;
            break;            
        case "FAILED":
        	logAsStatus = LogAs.FAILED;
            break;
       }                	
        return logAsStatus;
 }
	*/
	
	/* Code added by Krishan ends here */
	public void fnLanguageConverter(String strPrintMessage) throws UnsupportedEncodingException{
		// This function will convert language into printable format on eclipse console.
		
		PrintStream sysout = new PrintStream(System.out, true, "UTF-8");
		
		sysout.println(strPrintMessage);
				
	}
	
	public void fnCheckCountrySelectionAjaxCall(){
		
		// This will validate Ajax call on country selection on dispatch page.
		
		try{
			driver.navigate();
			Assert.assertTrue(fnisElementPresent(Constants.WebMachine_DispatchPage.SelectCountry_Header_Link), "Error! Country selection dropbox not found");
			fnClick(SelectCountry_Header_Link);
			Assert.assertTrue(fnisElementPresent(Constants.WebMachine_CommonConstants.SELogoHeader_Link), "Error! Ajax call is reloading the page");
			Reporter.log("Ajax call is not reloading the page, validation successfull", true);
			//////ATUReports.add("Ajax call validation successfull!", LogAs.PASSED, new CaptureScreen(ScreenshotOf.BROWSER_PAGE));
			
		}catch(Throwable t){
			
			ErrorUtil.addVerificationFailure(t);
			Reporter.log("Error! Ajax call is reloading the page, validation Failed", true);
			//////ATUReports.add("Error! Ajax call validation failed.", LogAs.FAILED, new CaptureScreen(ScreenshotOf.BROWSER_PAGE));
			//driver.navigate().back();
			
		}
		
		driver.navigate().refresh();
	}

	
	public void fnCheckCanonicalURL() throws UnsupportedEncodingException{
	
		// This fuction will check availability of Canonical url on page.
		
		try{
			Assert.assertTrue(fnisElementPresent(Constants.WebMachine_CommonConstants.CanonicalURL_Check), "Error! Canonical URL Not found for page : "+ driver.getTitle());
			Reporter.log("Canonical URL is available for page : "+ driver.getTitle(), false);
			fnLanguageConverter("Canonical URL is available for page : "+ driver.getTitle());
			Reporter.log("Canonical Url : "+ CanonicalURL_Check.getAttribute("href"), false);
			fnLanguageConverter("Canonical Url : "+ CanonicalURL_Check.getAttribute("href"));
		}catch(Throwable t){
			
			ErrorUtil.addVerificationFailure(t);
			Reporter.log("Error! Canonical URL Not found for page : "+ driver.getTitle(), false);
			fnLanguageConverter("Error! Canonical URL Not found for page : "+ driver.getTitle());
		}
		
	}	
	public void fnValidate_HomeHorizonatlNavigation(){
		
		// This will validate horizontal navigation menu on @Home section pages.
		
		Reporter.log("Validating Horizonatl navigation pane on @Home pages", true);
		try{
			Assert.assertTrue(HomeHeaderHorizontalMenu_Links.size()!=0, "Error! No Top menu found on Header of @Home section.");
			Reporter.log("Total tabs in horizonatl navigation pane of @Home header available for Country : " +CountryName_Link.getText()+" = "+ HomeHeaderHorizontalMenu_Links.size(), true);
			fnCheckCanonicalURL();
			for(int i=1;i<=HomeHeaderHorizontalMenu_Links.size();i++){
				try{
						String strTabName = driver.findElement(By.xpath(Constants.WebMachine_HomeLandingPage.HomeHeaderHorizontalMenu_Links+"["+i+"]/a")).getText();
						Reporter.log("Tab "+ i + " : " +strTabName, false);
						fnLanguageConverter("Tab "+ i + " : " +strTabName);
						String strClassAttr = driver.findElement(By.xpath(Constants.WebMachine_HomeLandingPage.HomeHeaderHorizontalMenu_Links+"["+i+"]")).getAttribute("class");
						//System.out.println(strClassAttr);
					
						if(strClassAttr.equalsIgnoreCase("dropdown")){
							
							Reporter.log("Tab "+i+" : "+ strTabName +" contains sub menu with following options", false);
							fnLanguageConverter("Tab "+i+" : "+ strTabName +" contains sub menu with following options");
							List<WebElement> eleTabsOptions = driver.findElements(By.xpath(Constants.WebMachine_HomeLandingPage.HomeHeaderHorizontalMenu_Links+"["+i+"]/div/ul/li"));
							Reporter.log("Total options in "+ strTabName+" menu :  "+ eleTabsOptions.size(), false);
							fnLanguageConverter("Total options in "+ strTabName+" menu :  "+ eleTabsOptions.size());
							for(int j=1;j<=eleTabsOptions.size();j++){
								
								String strSubMenuOptionName = driver.findElement(By.xpath(Constants.WebMachine_HomeLandingPage.HomeHeaderHorizontalMenu_Links+"["+i+"]/div/ul/li["+j+"]//a")).getAttribute("title");
								Reporter.log("Sub menu option "+j+ " : "+ strSubMenuOptionName , false);
								fnLanguageConverter("Sub menu option "+j+ " : "+ strSubMenuOptionName );
							}
							
						}
				
				}catch(Throwable e){
					
					ErrorUtil.addVerificationFailure(e);
					Reporter.log("Error! Validation failed for @Home horizonatl navigation pane", true);
				}
				
			}
			
			// Now validate each tab on horizontal navigation pane
			
			for(int i=1;i<=HomeHeaderHorizontalMenu_Links.size();i++){
				try{
						
						String strTabName = driver.findElement(By.xpath(Constants.WebMachine_HomeLandingPage.HomeHeaderHorizontalMenu_Links+"["+i+"]/a")).getText();
						Reporter.log("Validating Tab "+ i + " : " +strTabName, false);
						fnLanguageConverter("Validating Tab "+ i + " : " +strTabName);
						driver.findElement(By.xpath(Constants.WebMachine_HomeLandingPage.HomeHeaderHorizontalMenu_Links+"["+i+"]/a")).click();
						Assert.assertTrue(fnValidateTitle(), "Error! Validation failed for horizontal tab "+strTabName);
				        Reporter.log("Validation successful for horizontal tab : "+i + "  "+ strTabName, false);
				        fnLanguageConverter("Validation successful for horizontal tab : "+i + "  "+ strTabName);
				        fnCheckCanonicalURL();
				        String strClassAttr = driver.findElement(By.xpath(Constants.WebMachine_HomeLandingPage.HomeHeaderHorizontalMenu_Links+"["+i+"]")).getAttribute("class");
						driver.navigate().back();
						if(strClassAttr.equalsIgnoreCase("dropdown")){
							
							Reporter.log("Tab "+i+" : "+ strTabName +" contains sub menu with following options", true);
							List<WebElement> eleTabsOptions = driver.findElements(By.xpath(Constants.WebMachine_HomeLandingPage.HomeHeaderHorizontalMenu_Links+"["+i+"]/div/ul/li"));
							Reporter.log("Total options in "+ strTabName+" menu :  "+ eleTabsOptions.size(), true);
							/*for(int j=1;j<=eleTabsOptions.size();j++){
								try{
								String strSubMenuOptionName = driver.findElement(By.xpath(Constants.WebMachine_HomeLandingPage.HomeHeaderHorizontalMenu_Links+"["+i+"]/div/ul/li["+j+"]/div/a")).getAttribute("title");
								Reporter.log("Validating Sub menu option "+j+ " : "+ strSubMenuOptionName , true);
								driver.findElement(By.xpath(Constants.WebMachine_HomeLandingPage.HomeHeaderHorizontalMenu_Links+"["+i+"]/div/ul/li["+j+"]/div/a")).click();
								 Assert.assertTrue(fnValidateTitle(), "Error! Validation failed for sub menu option  "+strSubMenuOptionName + " under "+ strTabName);
							     Reporter.log("Validation successful for sub menu option  "+strSubMenuOptionName + " under "+ strTabName, true);
								}catch(Throwable t){
									ErrorUtil.addVerificationFailure(t);
									Reporter.log("Error! Validation failed for sub menu option",true);
								}
							}*/
							
						}
				        
				}catch(Throwable e){
					driver.navigate().back();
					ErrorUtil.addVerificationFailure(e);
					Reporter.log("Error! Validation failed for @Home horizonatl navigation pane", true);
				}
				
			}
			
			
			
		}catch(Throwable t){
			
			ErrorUtil.addVerificationFailure(t);
			Reporter.log("Error! Validation failed for @Home horizonatl navigation pane", true);
			
		}
		
		
	}
	public void fnValidate_HomeHeader(){
		
		// This will validate the header of @Home sections pages.
		Reporter.log("Validating Top menu on @Home header", true);
		try{
			
			Assert.assertTrue(fnisElementPresent(Constants.WebMachine_DispatchPage.ZoneA_Link), "@Home Section Not found on dispatch page.");
			fnClick(ZoneA_Link);
			//System.out.println("we are on @Home landing page..not lets validate header");
			Reporter.log("@Home landing page for country : " + CountryName_Link.getText(), true);
			Assert.assertTrue(HomeHeaderTopMenu_Links.size()!=0, "Error! No Top menu found on Header of @Home section.");
			Reporter.log("Total links in top menu of @Home header available for Country : " +CountryName_Link.getText()+" = "+ HomeHeaderTopMenu_Links.size(), true);
			
			for(int i=1;i<=HomeHeaderTopMenu_Links.size();i++){
				try{
						Reporter.log("Link "+ i + " : " + driver.findElement(By.xpath(Constants.WebMachine_HomeLandingPage.HomeHeaderTopMenu_Links+"["+i+"]/a")).getText(), true);
				}catch(Throwable e){
					
					ErrorUtil.addVerificationFailure(e);
					Reporter.log("Error! Validation failed for @Home header top menu", true);
				}
				
			}
			// Now lets click on each link and validate its functionality.
			
			for(int i=1;i<=HomeHeaderTopMenu_Links.size();i++){
				try{
						String strLinkName = driver.findElement(By.xpath(Constants.WebMachine_HomeLandingPage.HomeHeaderTopMenu_Links+"["+i+"]/a")).getText();
						Reporter.log("Validating Link "+ i + " : " +"  "+strLinkName, false);
						fnLanguageConverter("Validating Link "+ i + " : " +"  "+strLinkName);
						driver.findElement(By.xpath(Constants.WebMachine_HomeLandingPage.HomeHeaderTopMenu_Links+"["+i+"]/a")).click();
						fnCheckCanonicalURL();
						ArrayList<String> tabNew = new ArrayList<String> (driver.getWindowHandles());
						// driver.close();
					    int intTotalhandles = tabNew.size();
					  //  System.out.println("total handles is :" + intTotalhandles);
					    if(intTotalhandles!=1){
					    	fnSwitchToNewWindow();
					        Assert.assertTrue(fnValidateTitle(), "Error! Validation failed for landing page of "+strLinkName);
					        Reporter.log("Validation successful for link : "+i + "  "+ strLinkName, false);
					        fnLanguageConverter("Validation successful for link : "+i + "  "+ strLinkName);
					        //////ATUReports.add("Validation successful for link : "+i + "  "+ strLinkName, LogAs.PASSED, new CaptureScreen(ScreenshotOf.BROWSER_PAGE));
					        driver.close();
					        fnSwitchtoMainwindow();
					    }else{
					    	fnSwitchtoMainwindow();
					    	Assert.assertTrue(fnValidateTitle(), "Error! Validation failed for landing page of "+strLinkName);
					    	 Reporter.log("Validation successful for link : "+i + "  "+ strLinkName, false);
					    	 fnLanguageConverter("Validation successful for link : "+i + "  "+ strLinkName);
					    	 
					    	 driver.navigate().back();
					    }
					    
				}catch(Throwable e){
					
					ErrorUtil.addVerificationFailure(e);
					Reporter.log("Error! Validation failed for top menu link : "+i, true);
				}
				
			}
			
			
			
		}catch(Throwable t){
			
			ErrorUtil.addVerificationFailure(t);
			Reporter.log("Error! Validation failed for @Home header top menu", true);
		}
		
		
	}
	
	public boolean fnValidateTitle(){
		
		String StrCurrentURL = driver.getCurrentUrl();
		Reporter.log("Landing page URL : "+StrCurrentURL, true);
		if(driver.getTitle().contains("Error") || driver.getTitle().contains("404")){
			return false;
		}else{
			
			return true;
		}
		
	}
	
	public void fnValidate_SE_Logo(){
		
		// This will validate SE logo availability on each page and on click it takes user on Dispatch page.
		
		try{
			
			Assert.assertTrue(fnisElementPresent(Constants.WebMachine_CommonConstants.SELogoHeader_Link), "SE logo NOT Found");
			String strAltText = SELogoHeader_Link.getAttribute("alt");
			Reporter.log("SE logo is Available with alt text : "+ strAltText, false);
			fnLanguageConverter("SE logo is Available with alt text : "+ strAltText);
			fnClick(SELogoHeader_Link);
		//	System.out.println("cssvalue :   " + SELogoHeader_Link.getCssValue("font"));
			Assert.assertTrue(fnisElementPresent(Constants.WebMachine_DispatchPage.DispatchZones), "Error! SE logo link not taking user on Dispatch page");
			Reporter.log("Dispatch page title : "+ driver.getTitle(), false);
			fnLanguageConverter("Dispatch page title : "+ driver.getTitle());
			Reporter.log("SE logo on click is taking user on Dispatch page", true);
			//////ATUReports.add("Validation Passed for SE logo", LogAs.PASSED, new CaptureScreen(ScreenshotOf.BROWSER_PAGE));
			
			
		}catch(Throwable t){
			
			ErrorUtil.addVerificationFailure(t);
			Reporter.log("SE logo functionality not working", true);
			//////ATUReports.add("Error! Validation failed for SE logo", LogAs.FAILED, new CaptureScreen(ScreenshotOf.BROWSER_PAGE));
		}
		
		
	}
	
		
	public void fnValidate_Country(){
		// This will assert if country is changing on its own when user do search or clicks on any links
		// TBD when legacy page for search page moved to SDL.
		try{
			
			Reporter.log("Validating country check...", true);
			
			
		}catch(Throwable t){
			
			
			
		}
		
		
	}
	
	
	public void fnValidate_Search(){
		
		// This function will validate Search functionality on header of all the pages of WebMachine
		
		try{
			
			Reporter.log("Validating Search Functionality..", true);
			driver.navigate().refresh();
			fnCheckCanonicalURL();
			String strCountryName = CountryName_Link.getText();
			Search_Inputbox.sendKeys("energy");
			SearchIcon_Button.click();
			Assert.assertTrue(fnisElementPresent(Constants.WebMachine_CommonConstants.SearchResultPage_Check), "Search results page is Not displayed");
			Reporter.log("Title of Search landing page : "+ driver.getTitle(), false);
			fnLanguageConverter("Title of Search landing page : "+ driver.getTitle());
			Reporter.log("Search results page displayed succesfully! ", true);
			//////ATUReports.add("Validation Passed for Search results page", LogAs.PASSED, new CaptureScreen(ScreenshotOf.BROWSER_PAGE));
			fnCheckCanonicalURL();
			try{
				
				String strLandingPageCountry = LandingPageCountryName.getText();
				Assert.assertTrue(strCountryName.equalsIgnoreCase(strLandingPageCountry), "Error! Country is changing from "+ strCountryName+" to "+ strLandingPageCountry);
				
			}catch(Throwable e){
				//////ATUReports.add("Error! Country is Changing, Validation Failed for Search results page", LogAs.FAILED, new CaptureScreen(ScreenshotOf.BROWSER_PAGE));
				Reporter.log("Error! Country is changing", true);
				ErrorUtil.addVerificationFailure(e);
			}
			driver.navigate().back();
		}catch(Throwable t){
			Reporter.log("Error in validating Search functionality", true);
			ErrorUtil.addVerificationFailure(t);
			
		}
		
	}
	
	public void fnValidate_HeaderSocialMedia(){
		
		// This function validates social media links on the header of Dispatch page.
		
		// Validate FB and Twitter social media options on header of Dispatch page.
		
		try{
			
			Assert.assertTrue(fnisElementPresent(Constants.WebMachine_CommonConstants.HeaderSocial_Links), "Error! Social Media links for Facebook and Twitter NOT found on Header of Dispatch page");
			
			String strlink1 = driver.findElement(By.xpath(Constants.WebMachine_CommonConstants.HeaderSocial_Links+"[1]/span")).getAttribute("class");
			Reporter.log("Social Media links available on header of dispatch page : "+strlink1, false);
			fnLanguageConverter("Social Media links available on header of dispatch page : "+strlink1);
			String strlink2 = driver.findElement(By.xpath(Constants.WebMachine_CommonConstants.HeaderSocial_Links+"[2]/span")).getAttribute("class");
			Reporter.log("Social Media links available on header of dispatch page : "+strlink2, false);
			fnLanguageConverter("Social Media links available on header of dispatch page : "+strlink2);
			
			try{
				
				// Now validate if FB and Twitter links are working fine.
					String strBaseURL = "";
					Reporter.log("Going to validate link : " + strlink1, false);
					fnLanguageConverter("Going to validate link : " + strlink1);
					driver.findElement(By.xpath(Constants.WebMachine_CommonConstants.HeaderSocial_Links+"[1]")).click();
				
					Reporter.log("Landing page URL : "+driver.getCurrentUrl(), false);
					fnLanguageConverter("Landing page URL : "+driver.getCurrentUrl());
					boolean bolURL = strBaseURL.equalsIgnoreCase(driver.getCurrentUrl());						
					//System.out.println("bolURL : "+bolURL);
					
					if(!bolURL){
						if( driver.getTitle().contains("Error") ||  driver.getTitle().contains("404")){
							
							try{
								Assert.fail("Error! Link "+ strlink1+" is Broken");
								}catch(Throwable t){
									Reporter.log("Error! Link " +" : " +strlink1+ " is Broken", false);
									fnLanguageConverter("Error! Link " +" : " +strlink1+ " is Broken");
									ErrorUtil.addVerificationFailure(t);
									driver.navigate().back();
								}
						}else{
							
							
							try{
								
								Reporter.log("Social media link : " +strlink1 +" is working fine", false);
								fnLanguageConverter("Social media link : " +strlink1 +" is working fine");
								Reporter.log("Title of landing page : " +driver.getTitle(), false);
								fnLanguageConverter("Title of landing page : " +driver.getTitle());
								driver.navigate().back();
							}catch(Throwable t){
								Reporter.log("Social media link : " +strlink1 +" is NOT working", false);
								fnLanguageConverter("Social media link : " +strlink1 +" is NOT working");
								ErrorUtil.addVerificationFailure(t);
								
							}
							
						}
					
				}else{
					
					try{
						Assert.fail("Error! Link "+ strlink1+" is NOT working");
						}catch(Throwable t){
							Reporter.log("Error! Link " +" : " +strlink1+ " is NOT working", false);
							fnLanguageConverter("Error! Link " +" : " +strlink1+ " is NOT working");
							ErrorUtil.addVerificationFailure(t);
							//driver.navigate().back();
						}
				}
					
					Reporter.log("Going to click on link : " + strlink2, true);
					driver.findElement(By.xpath(Constants.WebMachine_CommonConstants.HeaderSocial_Links+"[2]")).click();
					
					Reporter.log("Landing page URL : "+driver.getCurrentUrl(), true);
					boolean bolURL2 = strBaseURL.equalsIgnoreCase(driver.getCurrentUrl());						
					//System.out.println("bolURL : "+bolURL2);
					
					if(!bolURL2){
						if( driver.getTitle().contains("Error") ||  driver.getTitle().contains("404")){
							
							try{
								Assert.fail("Error! Link "+ strlink2+" is Broken");
								}catch(Throwable t){
									Reporter.log("Error! Link " +" : " +strlink2+ " is Broken", false);
									fnLanguageConverter("Error! Link " +" : " +strlink2+ " is Broken");
									ErrorUtil.addVerificationFailure(t);
									driver.navigate().back();
								}
						}else{
							
							try{
								
								Reporter.log("Social media link : " +strlink2 +" is working fine", false);
								fnLanguageConverter("Social media link : " +strlink2 +" is working fine");
								Reporter.log("Title of landing page : " +driver.getTitle(), false);
								fnLanguageConverter("Title of landing page : " +driver.getTitle());
								driver.navigate().back();
								
							}catch(Throwable t){
								Reporter.log("Social media link : " +strlink2 +" is NOT working", false);
								fnLanguageConverter("Social media link : " +strlink2 +" is NOT working");
								ErrorUtil.addVerificationFailure(t);
								
							}
						}
					
				}else{
					
					try{
						Assert.fail("Error! Link "+ strlink2+" is NOT working");
						}catch(Throwable t){
							Reporter.log("Error! Link " +" : " +strlink2+ " is NOT working", false);
							fnLanguageConverter("Error! Link " +" : " +strlink2+ " is NOT working");
							ErrorUtil.addVerificationFailure(t);
							//driver.navigate().back();
						}
				}
					
					
					
					
				}catch(Throwable t){
					//////ATUReports.add("Error! Social media links on header of dispatch page for Facebook and Twitter NOT working", LogAs.FAILED, new CaptureScreen(ScreenshotOf.BROWSER_PAGE));
					Reporter.log("Social media links on header of dispatch page for Facebook and Twitter NOT working", true);
				}
				
			
			
		}catch(Throwable t){
			
			Reporter.log("Error! Social Media links for Facebook and Twitter NOT found on Header of Dispatch page", true);
			ErrorUtil.addVerificationFailure(t);
		}
		
		
	}
	
	
	public void fnValidate_HeaderSocialMoreOptions(){
		
		// This function validates social media options available under More menu on dispatch page header.
		
		try{
			
			// First check if More menu is available.
			//String strMoreOptionName ="";
			Assert.assertTrue(fnisElementPresent(Constants.WebMachine_CommonConstants.HeaderMoreSocial_Link), "Error! More menu for social media options NOT found on footer");
			
			// Now find number of social options available under More menu.
			
			List<WebElement> wbMoreOptions = driver.findElements(By.xpath(Constants.WebMachine_CommonConstants.HeaderMoreSocialOptions_Links));
			try{
				fnClick(HeaderMoreSocial_Link);
			Assert.assertTrue(wbMoreOptions.size()!=0, "Error! Social media options NOT found under More menu on dispatch page header");
			Reporter.log("Total social media options available under More menu on dispatch page header : " +wbMoreOptions.size(), true);
			for(int i=1;i<=wbMoreOptions.size();i++){
			
				// first print names of all options available then click each of them.
				String strMoreOptionName = driver.findElement(By.xpath(Constants.WebMachine_CommonConstants.HeaderMoreSocialOptions_Links+"["+i+"]/a")).getText();
				Reporter.log("Social media option : "+ i+ " "+strMoreOptionName, false);
				fnLanguageConverter("Social media option : "+ i+ " "+strMoreOptionName);
				
				}
			
			driver.navigate().refresh();
			
				// Now lets click on each social media option and validate.
				
				for( int j=1;j<=wbMoreOptions.size();j++){
					String strMoreOptionName = null;
					try{
					fnClick(HeaderMoreSocial_Link);
					String strBaseURL = driver.getCurrentUrl();
					strMoreOptionName = driver.findElement(By.xpath(Constants.WebMachine_CommonConstants.HeaderMoreSocialOptions_Links+"["+j+"]/a")).getText();
					Reporter.log("Going to click on Link : "+j+" "+strMoreOptionName, false);
					fnLanguageConverter("Going to click on Link : "+j+" "+strMoreOptionName);
					driver.findElement(By.xpath(Constants.WebMachine_CommonConstants.HeaderMoreSocialOptions_Links+"["+j+"]/a")).click();
					fnCheckCanonicalURL();
					Reporter.log("Landing page URL : "+driver.getCurrentUrl(), true);
					boolean bolURL2 = strBaseURL.equalsIgnoreCase(driver.getCurrentUrl());						
					//System.out.println("bolURL : "+bolURL2);
					
					if(!bolURL2){
						if( driver.getTitle().contains("Error") ||  driver.getTitle().contains("404")){
							
							try{
								Assert.fail("Error! Link "+ strMoreOptionName+" is Broken");
								}catch(Throwable t){
									Reporter.log("Error! Link " +" : " +strMoreOptionName+ " is Broken", false);
									fnLanguageConverter("Error! Link " +" : " +strMoreOptionName+ " is Broken");
									ErrorUtil.addVerificationFailure(t);
									driver.navigate().back();
								}
						}else{
							try{
								//Assert.assertTrue(driver.findElement(By.linkText("Schneider Electric")).isDisplayed(),"Social media link : " +strlink2 +" is NOT working");
								Reporter.log("Title of landing page : " +driver.getTitle(), false);
								fnLanguageConverter("Title of landing page : " +driver.getTitle());
								Reporter.log("Social media link : " +strMoreOptionName +" is working fine", false);
								fnLanguageConverter("Social media link : " +strMoreOptionName +" is working fine");
								driver.navigate().back();
								fnNavigate();
							}catch(Throwable t){
								Reporter.log("Social media link : " +strMoreOptionName +" is NOT working", false);
								fnLanguageConverter("Social media link : " +strMoreOptionName +" is NOT working");
								
								ErrorUtil.addVerificationFailure(t);
								//driver.navigate().back();
							}
							
						}
					
					
					}else{
						
						try{
							Assert.fail("Error! Link "+ strMoreOptionName+" is NOT working");
							}catch(Throwable t){
								Reporter.log("Error! Link " +" : " +strMoreOptionName+ " is NOT working", false);
								fnLanguageConverter("Error! Link " +" : " +strMoreOptionName+ " is NOT working");
								ErrorUtil.addVerificationFailure(t);
								//driver.navigate().back();
							}
					}
				
					}catch(Throwable t){
						
						Reporter.log("Error! Social media link : "+j+ "  "+ strMoreOptionName +" Not working", false);
						fnLanguageConverter("Error! Social media link : "+j+ "  "+ strMoreOptionName +" Not working");
						try{
							Assert.fail("Error! Social media link : "+j+ "  "+ strMoreOptionName +" Not working");
						}catch(Throwable e){
							ErrorUtil.addVerificationFailure(e);
							driver.navigate().back();
							fnNavigate();
						}
						
					}
			}
			
			
			}catch(Throwable t){
				
				Reporter.log("Error! Social media option NOT found under More menu on dispatch page header", true);
				ErrorUtil.addVerificationFailure(t);
				
			}
			
			
		}catch(Throwable t){
			
			Reporter.log( "Error! More menu for social media options NOT found on dispatch page header", true);
			ErrorUtil.addVerificationFailure(t);
			
		}
		
		
		
	}
	
	
	
	public void fnValidate_FooterSocialMedia(){
		// This function will validate Social media icons functionality on footer of all the pages of WebMachine.
		
		// Validate FB and Twitter social media options on footer.
		
		try{
			Assert.assertTrue(fnisElementPresent(Constants.WebMachine_CommonConstants.FooterSocial_Links), "Error! Social Media links for Facebook and Twitter NOT found on footer");
	
			String strlink1 = driver.findElement(By.xpath(Constants.WebMachine_CommonConstants.FooterSocial_Links+"[1]")).getAttribute("title");
			Reporter.log("Social Media links available on footer : "+strlink1, false);
			fnLanguageConverter("Social Media links available on footer : "+strlink1);
			String strlink2 = driver.findElement(By.xpath(Constants.WebMachine_CommonConstants.FooterSocial_Links+"[2]")).getAttribute("title");
			Reporter.log("Social Media links available on footer : "+strlink2, false);
			fnLanguageConverter("Social Media links available on footer : "+strlink2);
			
			try{
			
			// Now validate if FB and Twitter links are working fine.
				String strBaseURL = driver.getCurrentUrl();
				Reporter.log("Validating link : " + strlink1, false);
				fnLanguageConverter("Validating link : " + strlink1);
				driver.findElement(By.xpath(Constants.WebMachine_CommonConstants.FooterSocial_Links+"[1]")).click();
				Reporter.log("Landing page URL : "+driver.getCurrentUrl(), true);
				boolean bolURL = strBaseURL.equalsIgnoreCase(driver.getCurrentUrl());						
				//System.out.println("bolURL : "+bolURL);
				
				if(!bolURL){
					if( driver.getTitle().contains("Error") ||  driver.getTitle().contains("404")){
						
						try{
							Assert.fail("Error! Link "+ strlink1+" is Broken");
							}catch(Throwable t){
								Reporter.log("Error! Link " +" : " +strlink1+ " is Broken", false);
								fnLanguageConverter("Error! Link " +" : " +strlink1+ " is Broken");
								ErrorUtil.addVerificationFailure(t);
								driver.navigate().back();
							}
					}else{
						
						
						try{
							//System.out.println(driver.findElements(By.linkText("Schneider Electric")).size()!=0);
							//Assert.assertTrue(driver.findElements(By.linkText("Schneider Electric")).size()!=0,"Social media link : " +strlink2 +" is NOT working");
							Reporter.log("Social media link : " +strlink1 +" is working fine", false);
							fnLanguageConverter("Social media link : " +strlink1 +" is working fine");
							Reporter.log("Title of landing page : " +driver.getTitle(), false);
							fnLanguageConverter("Title of landing page : " +driver.getTitle());
							driver.navigate().back();
						}catch(Throwable t){
							Reporter.log("Social media link : " +strlink1 +" is NOT working", false);
							fnLanguageConverter("Social media link : " +strlink1 +" is NOT working");
							ErrorUtil.addVerificationFailure(t);
							
						}
						
					}
				
			}else{
				
				try{
					Assert.fail("Error! Link "+ strlink1+" is NOT working");
					}catch(Throwable t){
						Reporter.log("Error! Link " +" : " +strlink1+ " is NOT working", false);
						fnLanguageConverter("Error! Link " +" : " +strlink1+ " is NOT working");
						ErrorUtil.addVerificationFailure(t);
						//driver.navigate().back();
					}
			}
				
				Reporter.log("Going to click on link : " + strlink2, true);
				driver.findElement(By.xpath(Constants.WebMachine_CommonConstants.FooterSocial_Links+"[2]")).click();
				Reporter.log("Landing page URL : "+driver.getCurrentUrl(), true);
				boolean bolURL2 = strBaseURL.equalsIgnoreCase(driver.getCurrentUrl());						
				//System.out.println("bolURL : "+bolURL2);
				
				if(!bolURL2){
					if( driver.getTitle().contains("Error") ||  driver.getTitle().contains("404")){
						
						try{
							Assert.fail("Error! Link "+ strlink2+" is Broken");
							}catch(Throwable t){
								Reporter.log("Error! Link " +" : " +strlink2+ " is Broken", false);
								fnLanguageConverter("Error! Link " +" : " +strlink2+ " is Broken");
								ErrorUtil.addVerificationFailure(t);
								driver.navigate().back();
							}
					}else{
						
						try{
							//Assert.assertTrue(driver.findElement(By.linkText("Schneider Electric")).isDisplayed(),"Social media link : " +strlink2 +" is NOT working");
							Reporter.log("Social media link : " +strlink2 +" is working fine", false);
							fnLanguageConverter("Social media link : " +strlink2 +" is working fine");
							Reporter.log("Title of landing page : " +driver.getTitle(), false);
							fnLanguageConverter("Title of landing page : " +driver.getTitle());
							driver.navigate().back();
						}catch(Throwable t){
							Reporter.log("Social media link : " +strlink2 +" is NOT working", false);
							fnLanguageConverter("Social media link : " +strlink2 +" is NOT working");
							ErrorUtil.addVerificationFailure(t);
							
						}
					}
				
			}else{
				
				try{
					Assert.fail("Error! Link "+ strlink2+" is NOT working");
					}catch(Throwable t){
						Reporter.log("Error! Link " +" : " +strlink2+ " is NOT working", false);
						fnLanguageConverter("Error! Link " +" : " +strlink2+ " is NOT working");
						ErrorUtil.addVerificationFailure(t);
						//driver.navigate().back();
					}
			}
				
				
				
				
			}catch(Throwable t){
				
				Reporter.log("Social media links for Facebook and Twitter NOT working", true);
			}
			
		}catch(Throwable t){
		
			Reporter.log("Error! Social Media links for Facebook and Twitter NOT found on footer", true);
			ErrorUtil.addVerificationFailure(t);
		}
		
	}
	
	
	public void fnValidate_FooterMoreSocialOptions(){
		
		// This function will validate social options available under more menu on footer.
		
		try{
			
			// First check if More menu is available.
			//String strMoreOptionName ="";
			Assert.assertTrue(fnisElementPresent(Constants.WebMachine_CommonConstants.FooterMoreSocial_Link), "Error! More menu for social media options NOT found on footer");
			
			// Now find number of social options available under More menu.
			
			List<WebElement> wbMoreOptions = driver.findElements(By.xpath(Constants.WebMachine_CommonConstants.FooterMoreSocialOptions_Links));
			try{
				fnClick(FooterMoreSocial_Link);
			Assert.assertTrue(wbMoreOptions.size()!=0, "Error! Social media optione NOT found under More menu");
			Reporter.log("Total social media options available under More menu : " +wbMoreOptions.size(), true);
			for(int i=1;i<=wbMoreOptions.size();i++){
			
				// first print names of all options available then click each of them.
				String strMoreOptionName = driver.findElement(By.xpath(Constants.WebMachine_CommonConstants.FooterMoreSocialOptions_Links+"["+i+"]/a")).getText();
				Reporter.log("Social media option : "+ i+ " "+strMoreOptionName, false);
				fnLanguageConverter("Social media option : "+ i+ " "+strMoreOptionName);
				
				}
			driver.navigate().refresh();
				
				// Now lets click on each social media option and validate.
				
				for( int j=1;j<=wbMoreOptions.size();j++){
					String strMoreOptionName = "";
					try{
					fnClick(FooterMoreSocial_Link);
					String strBaseURL ="";
					strMoreOptionName = driver.findElement(By.xpath(Constants.WebMachine_CommonConstants.FooterMoreSocialOptions_Links+"["+j+"]/a")).getText();
					Reporter.log("Validating Link : "+j+" "+strMoreOptionName, false);
					fnLanguageConverter("Validating Link : "+j+" "+strMoreOptionName);
					driver.findElement(By.xpath(Constants.WebMachine_CommonConstants.FooterMoreSocialOptions_Links+"["+j+"]/a")).click();
					Reporter.log("Landing page URL : "+driver.getCurrentUrl(), true);
					boolean bolURL2 = strBaseURL.equalsIgnoreCase(driver.getCurrentUrl());						
					//System.out.println("bolURL : "+bolURL2);
					
					if(!bolURL2){
						if( driver.getTitle().contains("Error") ||  driver.getTitle().contains("404")){
							
							try{
								Assert.fail("Error! Link "+ strMoreOptionName+" is Broken");
								}catch(Throwable t){
									Reporter.log("Error! Link " +" : " +strMoreOptionName+ " is Broken", false);
									fnLanguageConverter("Error! Link " +" : " +strMoreOptionName+ " is Broken");
									ErrorUtil.addVerificationFailure(t);
									driver.navigate().back();
								}
						}else{
							try{
								//Assert.assertTrue(driver.findElement(By.linkText("Schneider Electric")).isDisplayed(),"Social media link : " +strlink2 +" is NOT working");
								Reporter.log("Title of landing page : " +driver.getTitle(), false);
								fnLanguageConverter("Title of landing page : " +driver.getTitle());
								Reporter.log("Social media link : " +strMoreOptionName +" is working fine", false);
								fnLanguageConverter("Social media link : " +strMoreOptionName +" is working fine");
								driver.navigate().back();
							}catch(Throwable t){
								Reporter.log("Social media link : " +strMoreOptionName +" is NOT working", true);
								ErrorUtil.addVerificationFailure(t);
								
							}
							
						}
					
					
					}else{
						
						try{
							Assert.fail("Error! Link "+ strMoreOptionName+" is NOT working");
							}catch(Throwable t){
								Reporter.log("Error! Link " +" : " +strMoreOptionName+ " is NOT working", false);
								fnLanguageConverter("Error! Link " +" : " +strMoreOptionName+ " is NOT working");
								ErrorUtil.addVerificationFailure(t);
								//driver.navigate().back();
							}
					}
				
					}catch(Throwable t){
						
						Reporter.log("Error! Social media link : "+j+ "  "+ strMoreOptionName +"Not working", false);
						fnLanguageConverter("Error! Social media link : "+j+ "  "+ strMoreOptionName +"Not working");
						try{
							Assert.fail("Error! Social media link : "+j+ "  "+ strMoreOptionName +"Not working");
						}catch(Throwable e){
							ErrorUtil.addVerificationFailure(e);
						}
						
					}
			}
			
			
			}catch(Throwable t){
				
				Reporter.log("Error! Social media option NOT found under More menu", true);
				ErrorUtil.addVerificationFailure(t);
				
			}
			
			
		}catch(Throwable t){
			
			Reporter.log( "Error! More menu for social media options NOT found on footer", true);
			ErrorUtil.addVerificationFailure(t);
			
		}
		
		
	}
	

	

	public void fnValidateFooter_LinksSection(){
		
		// This function validates the availability of Home, Work and Partners section/column links on footer.
		
		// First find out number of sections available on footer.
		
		try{
			
			String strZoneName = "";
			List<WebElement> wbFooter_Links= driver.findElements(By.xpath(Constants.WebMachine_CommonConstants.FooterSection_Links));
			Assert.assertTrue((wbFooter_Links.size() != 0),"Zones column NOT found on footer");
			
			Reporter.log("Total zones column found on footer : " + wbFooter_Links.size(), true);
			
			for(int i=1;i<=wbFooter_Links.size();i++){
				try{
				 strZoneName = driver.findElement(By.xpath(Constants.WebMachine_CommonConstants.FooterSection_Links+"["+i+"]"+"/a")).getText();
				Reporter.log("Zone "+i+" : "+strZoneName, false);
				fnLanguageConverter("Zone "+i+" : "+strZoneName);
				List<WebElement> wbZone_Links = driver.findElements(By.xpath(Constants.WebMachine_CommonConstants.FooterSection_Links+"["+i+"]"+"/ul/li"));
				Assert.assertTrue((wbZone_Links.size() != 0),"Zone "+strZoneName+" column NOT found on footer");
				Reporter.log("Zone : "+strZoneName+" column found on footer with total links : " + wbZone_Links.size(), false);
				fnLanguageConverter("Zone : "+strZoneName+" column found on footer with total links : " + wbZone_Links.size());
				for(int j=1;j<=wbZone_Links.size();j++){
					String strZoneLinks = driver.findElement(By.xpath(Constants.WebMachine_CommonConstants.FooterSection_Links+"["+i+"]"+"/ul/li["+j+"]/a")).getText();
					Reporter.log("Link "+j+" : "+strZoneLinks, false);
					fnLanguageConverter("Link "+j+" : "+strZoneLinks);
									
				}
				
				
			}catch(Throwable t){
				
				ErrorUtil.addVerificationFailure(t);
				Reporter.log("Zone "+ strZoneName +" column NOT found on footer", true);
				
			}				
			}
		}catch(Throwable t){
			Reporter.log("Zones column NOT found on footer", true);
			ErrorUtil.addVerificationFailure(t);
		}
		
		
		// Now check number of links available under each zone column with their names and then click on them to check functionality.
		
		try{
			
			String strZoneName = "";
			List<WebElement> wbFooter_Links= driver.findElements(By.xpath(Constants.WebMachine_CommonConstants.FooterSection_Links));
			Assert.assertTrue((wbFooter_Links.size() != 0),"Zones column NOT found on footer");
			
			//Reporter.log("Total zones column found on footer : " + wbFooter_Links.size(), true);
			
			for(int i=1;i<=wbFooter_Links.size();i++){
				try{
				 strZoneName = driver.findElement(By.xpath(Constants.WebMachine_CommonConstants.FooterSection_Links+"["+i+"]"+"/a")).getText();
				//Reporter.log("Zone "+i+" : "+strZoneName, true);
				List<WebElement> wbZone_Links = driver.findElements(By.xpath(Constants.WebMachine_CommonConstants.FooterSection_Links+"["+i+"]"+"/ul/li"));
				Assert.assertTrue((wbZone_Links.size() != 0),"Zone "+strZoneName+" column NOT found on footer");
				Reporter.log("Zone : "+strZoneName+" has total links : " + wbZone_Links.size(), true);
				for(int j=1;j<=wbZone_Links.size();j++){
					String strZoneLinks = driver.findElement(By.xpath(Constants.WebMachine_CommonConstants.FooterSection_Links+"["+i+"]"+"/ul/li["+j+"]/a")).getText();
					Reporter.log("Validating link "+j+" : "+strZoneLinks, false);
					fnLanguageConverter("Validating link "+j+" : "+strZoneLinks);
					String strBaseURL = driver.getCurrentUrl();
					try{
					driver.findElement(By.xpath(Constants.WebMachine_CommonConstants.FooterSection_Links+"["+i+"]"+"/ul/li["+j+"]/a")).click();
					Reporter.log("Landing page URL : "+driver.getCurrentUrl(), true);
					fnCheckCanonicalURL();
					boolean bolURL = strBaseURL.equalsIgnoreCase(driver.getCurrentUrl());						
					//System.out.println("bolURL : "+bolURL);
					
					if(!bolURL){
						if( driver.getTitle().contains("Error") ||  driver.getTitle().contains("404")){
							//System.out.println("inside if2");
							
							try{
							Assert.fail("Error! Link "+ strZoneLinks+" is Broken");
							}catch(Throwable t){
								Reporter.log("Error! Link "+j +" : " +strZoneLinks+ " is Broken", false);
								fnLanguageConverter("Error! Link "+j +" : " +strZoneLinks+ " is Broken");
								ErrorUtil.addVerificationFailure(t);
							}
						}else{
							//System.out.println("inside else1");
							Reporter.log("Tittle of Landing page : " +driver.getTitle(), false);
							fnLanguageConverter("Tittle of Landing page : " +driver.getTitle());
							Reporter.log("Link "+j+" : "+strZoneLinks +" is working fine.", false);
							fnLanguageConverter("Link "+j+" : "+strZoneLinks +" is working fine.");
							driver.navigate().back();
						}
					}else{
						//System.out.println("inside else2");
						try{
							Assert.fail("Error! Link "+ strZoneLinks +" is NOT Working");
							}catch(Throwable t){
								Reporter.log("Error! Link "+i +" : " +strZoneLinks+ " is NOT working", false);
								fnLanguageConverter("Error! Link "+i +" : " +strZoneLinks+ " is NOT working");
								ErrorUtil.addVerificationFailure(t);
							}
						
					}
					
					}catch(Throwable t){
						
						Reporter.log("Error ! Link : "+strZoneLinks +" NOT found in Zone : "+strZoneName, false);
						fnLanguageConverter("Error ! Link : "+strZoneLinks +" NOT found in Zone : "+strZoneName);
						//////ATUReports.add("Error ! Link : "+strZoneLinks +" NOT found in Zone : "+strZoneName, LogAs.FAILED, new CaptureScreen(ScreenshotOf.BROWSER_PAGE));
						ErrorUtil.addVerificationFailure(t);
					}
																
			    }
				}catch(Throwable t){
				
				ErrorUtil.addVerificationFailure(t);
				Reporter.log("Zone "+ strZoneName +" column NOT found on footer", false);
				fnLanguageConverter("Zone "+ strZoneName +" column NOT found on footer");
				//////ATUReports.add("Error ! Zone "+ strZoneName +" column NOT found on footer ", LogAs.FAILED, new CaptureScreen(ScreenshotOf.BROWSER_PAGE));
			}				
				
			}
			
		}catch(Throwable t){
			
			ErrorUtil.addVerificationFailure(t);
			Reporter.log("Zones column NOT found on footer", true);
			//////ATUReports.add("Zones column NOT found on footer ", LogAs.FAILED, new CaptureScreen(ScreenshotOf.BROWSER_PAGE));
		}
		
		
	}
	
	public void fnValidate_FooterLegalsLinks(){
		
		// This function will validate all the SE legal information links on footer.
		
		// First count number of links in footer-legal section.
		
		try{
			
			
			List<WebElement> wbFooterLegal_Links= driver.findElements(By.xpath(Constants.WebMachine_CommonConstants.FooterLegal_Links));
			Reporter.log("Total links available under Legal links section : " + wbFooterLegal_Links.size(), true);
			for(int i=1;i<=wbFooterLegal_Links.size();i++){
				String strFooterLegalLinkName = driver.findElement(By.xpath(Constants.WebMachine_CommonConstants.FooterLegal_Links+"["+i+"]"+"/a")).getText();
				Reporter.log("Link "+i+" : "+strFooterLegalLinkName , false);
				fnLanguageConverter("Link "+i+" : "+strFooterLegalLinkName);
				
			}
			
			
		}catch(Throwable e){
			
			Reporter.log("Error! Footer-Legals section not found on footer", true);
			Assert.fail("Error! Footer-Legals section not found on footer");
			//////ATUReports.add("Error! Footer-Legals section not found on footer", LogAs.FAILED, new CaptureScreen(ScreenshotOf.BROWSER_PAGE));
		}
		
		// Now click on each link
		
		/*try{
			
			List<WebElement> wbFooterLegal_Links= driver.findElements(By.xpath(Constants.WebMachine_CommonConstants.FooterLegal_Links));
			Assert.assertTrue((wbFooterLegal_Links.size() != 0),"Footer-Legals section NOT found on footer");
			for(int i=1;i<=wbFooterLegal_Links.size();i++){
				String strFooterLegalLinkName = driver.findElement(By.xpath(Constants.WebMachine_CommonConstants.FooterLegal_Links+"["+i+"]"+"/a")).getText();
				Reporter.log("Going to click on Link "+i+" : "+ driver.findElement(By.xpath(Constants.WebMachine_CommonConstants.FooterLegal_Links+"["+i+"]"+"/a")).getText(), true);
				try{
					String strBaseURL = driver.getCurrentUrl();
					driver.findElement(By.xpath(Constants.WebMachine_CommonConstants.FooterLegal_Links+"["+i+"]"+"/a")).click();
					
					//Reporter.log("Landing page URL : "+driver.getCurrentUrl(), true);
					//if(i==2){
						ArrayList<String> tabNew = new ArrayList<String> (driver.getWindowHandles());
						// driver.close();
					    int intTotalhandles = tabNew.size();
					   // System.out.println("About us handles : "+ intTotalhandles);
					    if(intTotalhandles>1){
						fnSwitchToNewWindow();
					    }
				//	fnSwitchToNewWindow();
					//}
					//boolean bolURL = strBaseURL.equalsIgnoreCase(driver.getCurrentUrl());	
					//if(!bolURL){
						if( driver.getTitle().contains("Error") ||  driver.getTitle().contains("404")){
							
							try{
								Assert.fail("Error! Link "+ strFooterLegalLinkName+" is Broken");
								}catch(Throwable t){
									Reporter.log("Error! Link "+i +" : " +strFooterLegalLinkName+ " is Broken", true);
									ErrorUtil.addVerificationFailure(t);
								}
							}else{
								
								//System.out.println("inside else1");
							//	Reporter.log("Tittle of Landing page : " +driver.getTitle(), true);
								ArrayList<String> tabNew1 = new ArrayList<String> (driver.getWindowHandles());
								// driver.close();
							    int intTotalhandles1 = tabNew1.size();
							   // System.out.println("About us handles : "+ intTotalhandles);
							    if(intTotalhandles>1){
							    	fnSwitchtoMainwindow();
							    }
								
								Reporter.log("Link "+i+" : "+strFooterLegalLinkName +" is working fine.", true);
								//driver.navigate().back();
							}
						
					
				}catch(Throwable t){
					
					Reporter.log("Error! Footer-Legals section link : " +strFooterLegalLinkName+" NOT found on footer", true);
					//ErrorUtil.addVerificationFailure(t);
					try{
					Assert.fail("Error! Footer-Legals section link : " +strFooterLegalLinkName+" NOT found on footer");
					}catch(Throwable e){
						ErrorUtil.addVerificationFailure(e);
					}
				}
				
			}
			
			
		}catch(Throwable t){
			
			Reporter.log("Error! Footer-Legals section not found on footer", true);
			ErrorUtil.addVerificationFailure(t);
			
		}*/
		
	}
	
	

	public void fnValidate_FooterAboutLinks(){
		// This function will validate all the links under About column on footer of all the pages of WebMachine.
					
					
			// First count number of links available with title in About us column.
			try{
				
				List<WebElement> wbAboutUs_Links= driver.findElements(By.xpath(Constants.WebMachine_CommonConstants.AboutUSFooterColumn_Link));
				Assert.assertTrue((wbAboutUs_Links.size() != 0),"About us column NOT found on footer");
				Reporter.log("Total links available under About us column : " + wbAboutUs_Links.size(), true);
				for(int i=1;i<=wbAboutUs_Links.size();i++){
					
					
					String myString = driver.findElement(By.xpath(Constants.WebMachine_CommonConstants.AboutUSFooterColumn_Link+"["+i+"]"+"/a")).getText();
					
					Reporter.log("Link "+i+" : "+myString, false);
					fnLanguageConverter("Link "+i+" : "+myString);
					
				}				
				
			}catch(Throwable t){
				Reporter.log("Error! About Us column NOT found on footer", true);
				//////ATUReports.add("Error! About Us column NOT found on footer", LogAs.FAILED, new CaptureScreen(ScreenshotOf.BROWSER_PAGE));
				ErrorUtil.addVerificationFailure(t);
			}
			
			// Now click on each link in ABout Us column and check its functionality.
			
			try{
				
				List<WebElement> wbAboutUs_Links= driver.findElements(By.xpath(Constants.WebMachine_CommonConstants.AboutUSFooterColumn_Link));
				Assert.assertTrue((wbAboutUs_Links.size() != 0),"About us column NOT found on footer");
				//////ATUReports.add("About Us column found on footer", LogAs.PASSED, new CaptureScreen(ScreenshotOf.BROWSER_PAGE));
				for(int i=1;i<=wbAboutUs_Links.size();i++){
					
					try{
						String strBaseURL = driver.getCurrentUrl();
						Reporter.log("Going to Click on Link "+i+" : "+driver.findElement(By.xpath(Constants.WebMachine_CommonConstants.AboutUSFooterColumn_Link+"["+i+"]"+"/a")).getText(), true);
						String strLinkName = driver.findElement(By.xpath(Constants.WebMachine_CommonConstants.AboutUSFooterColumn_Link+"["+i+"]"+"/a")).getText();
						driver.findElement(By.xpath(Constants.WebMachine_CommonConstants.AboutUSFooterColumn_Link+"["+i+"]"+"/a")).click();
						ArrayList<String> tabNew = new ArrayList<String> (driver.getWindowHandles());
						// driver.close();
					    int intTotalhandles = tabNew.size();
					    //System.out.println("About us handles : "+ intTotalhandles);
					    if(intTotalhandles>1){
						fnSwitchToNewWindow();
					    }
						Reporter.log("Landing page URL : "+driver.getCurrentUrl(), true);
						fnCheckCanonicalURL();
						boolean bolURL = strBaseURL.equalsIgnoreCase(driver.getCurrentUrl());
						//System.out.println("bolURL : "+bolURL);
					if(!bolURL){
						//System.out.println("inside if1");
						if( driver.getTitle().contains("Error") ||  driver.getTitle().contains("404")){
							//System.out.println("inside if2");
							Reporter.log("Error! Link "+i +" : " +strLinkName+ " is Broken", false);
							fnLanguageConverter("Error! Link "+i +" : " +strLinkName+ " is Broken");
							Assert.fail("Error! Link "+ strLinkName+" is Broken");
						}else{
							//System.out.println("inside else1");
							Reporter.log("Link "+i+" : "+strLinkName+" is working fine.", false);
							fnLanguageConverter("Link "+i+" : "+strLinkName+" is working fine.");
						}
					}else{
						//System.out.println("inside else2");
						Reporter.log("Error! Link "+i+" : "+strLinkName+" NOT working", false);
						fnLanguageConverter("Error! Link "+i+" : "+strLinkName+" NOT working");
						//Assert.fail("Error! Link NOT working");
					}
					 if(intTotalhandles>1){
						 fnSwitchtoMainwindow();
						    }
					
					//driver.navigate().back();
					
				}catch(Throwable t){
					
					fnSwitchtoMainwindow();
					Reporter.log("Error! "+driver.findElement(By.xpath(Constants.WebMachine_CommonConstants.AboutUSFooterColumn_Link+"["+i+"]"+"/a")).getText()+" Link NOT found on footer", false);
					fnLanguageConverter("Error! "+driver.findElement(By.xpath(Constants.WebMachine_CommonConstants.AboutUSFooterColumn_Link+"["+i+"]"+"/a")).getText()+" Link NOT found on footer");
					try{
					Assert.fail("Error! Link "+driver.findElement(By.xpath(Constants.WebMachine_CommonConstants.AboutUSFooterColumn_Link+"["+i+"]"+"/a")).getText()+" NOT found on footer");
					}catch(Throwable e){
						ErrorUtil.addVerificationFailure(e);
						//System.out.println("Assert.fail logged");
					}
					}
				
			}
				
			}catch(Throwable t){
				
				Reporter.log("Error! About Us column NOT found on footer", true);
				//////ATUReports.add("Error! About Us column NOT found on footer", LogAs.FAILED, new CaptureScreen(ScreenshotOf.BROWSER_PAGE));
				ErrorUtil.addVerificationFailure(t);
			}
			
			
			fnNavigate();
		
			
	}
	
	public void fnSwitchToNewWindow(){
		try{
		   ArrayList<String> tabNew = new ArrayList<String> (driver.getWindowHandles());
			// driver.close();
		    int intTotalhandles = tabNew.size();
		    //System.out.println("total handles is :" + intTotalhandles);
			Reporter.log("Base window URL before switching : "+driver.getCurrentUrl(), true);
			 driver.switchTo().window(tabNew.get((intTotalhandles-1)));
			 fnWaitForLoad(driver);
			 Thread.sleep(5000);
			 Reporter.log("New window Title after switching : "+driver.getTitle(), false);
			 fnLanguageConverter("New window Title after switching : "+driver.getTitle());
			 Reporter.log("New window URL after switching : "+driver.getCurrentUrl(), true);
		}catch(Throwable t){
			Reporter.log("Error! Could not switch to the New Window", true);
			Assert.fail("Could not switch to the New Window");
		}
	}
	
	public void fnWaitForLoad(WebDriver driver) {
	//	System.out.println("waiting for page to load...");
	    ExpectedCondition<Boolean> pageLoadCondition = new
	        ExpectedCondition<Boolean>() {
	            public Boolean apply(WebDriver driver) {
	                return ((JavascriptExecutor)driver).executeScript("return document.readyState").equals("complete");
	            }
	        };
	        
	    WebDriverWait wait = new WebDriverWait(driver, 30);
	    wait.until(pageLoadCondition);
	}
	
	public void fnSwitchtoMainwindow(){
		try{
			 ArrayList<String> tabBase = new ArrayList<String> (driver.getWindowHandles());
			// Reporter.log("Current window URL before switching : "+driver.getCurrentUrl(), true);
			 int intotalhandles = tabBase.size();
			 for(int i=intotalhandles;i!=1;i--){
				 driver.switchTo().window(tabBase.get(intotalhandles-1));
			 driver.close();
			 }
			 driver.switchTo().window(tabBase.get(0));
			// Reporter.log("Base window URL After switching : "+driver.getCurrentUrl(), true);
			
			
		}catch(Exception e){
			Reporter.log("Could not switch back to base Window", true);
			Assert.fail("Could not switch back to the base Window");
			
		}
	}
	
	
	

	
	public void fnSelectDropboxValue(WebElement wbdropbox, String strValue){
		// This function will select value from dropdown box.
		Reporter.log("Going to select : " + strValue + "  from dropdown menu :  " + wbdropbox, true);
		try{
		Select dropdown = new Select(wbdropbox);
		dropdown.selectByVisibleText(strValue);
		}catch(Throwable t){
			Reporter.log("Error ! Couldn't select value : "  + strValue + " from dropbox : " + wbdropbox, true);
			Assert.fail("Error ! Couldn't select value from dropbox");
		}
		
		
	}
	
	public void fnInputbox(WebElement wbInputbox, String strValue){
		// This will enter value into Inputbox.
		Reporter.log("Going to Enter : " + strValue + "  in Inputbox :  " + wbInputbox, true);
		try{
			
			wbInputbox.sendKeys(strValue);
			
		}catch(Throwable t){
			Reporter.log("Error ! Couldn't enter value : "  + strValue + " in Inputbox : " + wbInputbox, true);
			Assert.fail("Error ! Couldn't enter value in Inputbox");
			
		}
		
		
		
	}
	
	
	public boolean fnClick(WebElement wbClickOn){
		//  This function will click on WebButton
	//	Reporter.log("Going to Click on :  " + wbClickOn , true);
		try{
			
			wbClickOn.click();
			return true;
		}catch(Throwable t){
			Reporter.log("Error ! Couldn't Click on : " + wbClickOn, true);
			Assert.fail("Error ! Click action failed");
			return false;
		}
	}
	

	public boolean fnisElementPresent(String Xpath){
	    
		 //List<WebElement> Webele = driver.findElements(by);
		 //int count = Webele.size();
		// return driver.findElements(by).size() != 0;
		
		return driver.findElements(By.xpath(Xpath)).size()!=0;
		 
	}
	
	@SuppressWarnings("unused")
	protected WebMachine_Base_Page(){      // Constructor is made protected 
		
		Config = new Properties(); 
		ENV = new Properties();
		//driver = null;
		
		try {
			
			FileInputStream fs = new FileInputStream(Constants.Paths.Config_FILE_PATH);
			if(fs==null){
				System.out.println("fs is null");
			}else
			Config.load(fs);
			// Init ENV Properties
			
			String filename = Config.getProperty("environment")+".properties";
			System.out.println("Environment Filename :"+ filename);
			//Reporter.log("Environment Filename :"+ filename, true);
			fs = new FileInputStream(Constants.Paths.Config_FOLDER_PATH+filename);
			ENV.load(fs);
		} catch (Exception e) {
				//e.printStackTrace();
			 System.out.println("Unable to load file input stream");
		} 
	}
	
	
	public String fnCloseDriver(){
		// This will close driver instances
		try{
			//driver.close();
			driver.quit();
			return "Pass";
		}catch(Exception e){
			return "Fail";
		}
	}
	
	public void fnLog(String mesg){
		APPLICATION_LOGS.debug(mesg);
	}
	public void fnOpenbrowser(String browser){
		System.out.println("browser name is-->"+browser);
		//browser = "Mozilla";
	// This will open browser
		//browser = "Mozilla";
		System.out.println("Selected browser :" +browser);
		if(browser.equalsIgnoreCase("Mozilla")&& backup_mozilla!=null){
			driver = backup_mozilla;
			return;
		}else if(browser.equalsIgnoreCase("Chrome")&& backup_chrome!=null){
			driver = backup_chrome;
			return;
		}else if(browser.equalsIgnoreCase("IE")&& backup_ie!=null){
			driver = backup_ie;
			return;
		}
			
	try{	
		//driver = new FirefoxDriver();
		if(browser.equalsIgnoreCase("Mozilla")){
			System.out.println("Running Scripts on Mozilla FireFox!");
			fnLog("Running Scripts on Mozilla FireFox!");
			cap = DesiredCapabilities.firefox();
			cap.setBrowserName("Mozilla"); // chro,iexplore
			cap.setPlatform(Platform.ANY);
			//driver = new RemoteWebDriver(new URL("http://10.155.37.57:4444/wd/hub"),cap);
			driver = new FirefoxDriver(cap);
		//	 ATUReports.setWebDriver(driver);
			backup_mozilla = driver;
			
		}else if(browser.equalsIgnoreCase("IE")){
			System.out.println("Running Scripts on Internet Explorer!");
			fnLog("Running Scripts on IE!");
			cap = DesiredCapabilities.internetExplorer(); // no need path of ie exe
			cap.setBrowserName("IE");
			cap.setPlatform(Platform.WINDOWS);
			driver = new RemoteWebDriver(new URL("http://localhost:4444/wd/hub"),DesiredCapabilities.internetExplorer());
			// ATUReports.setWebDriver(driver);
			//System.setProperty("webdriver.ie.driver",System.getProperty("user.dir")+"\\drivers\\IEDriverServer.exe");
			// driver = new InternetExplorerDriver();
		//	backup_ie = driver;
			
		}else if(browser.equalsIgnoreCase("Chrome")){
			//WebDriver driver = new ChromeDriver();
			System.out.println("Running Scripts on Chrome!");
			//System.setProperty("webdriver.chrome.driver",System.getProperty("user.dir")+"\\drivers\\chromedriver.exe");
			System.setProperty("webdriver.chrome.driver","D:\\softwares\\chromedriver_win32\\chromedriver.exe");

			cap = DesiredCapabilities.chrome(); // no need path of chrome exe
			cap.setJavascriptEnabled(true);
	
			cap.setBrowserName("googlechrome");
			//cap.setCapability("chrome.switches", Arrays.asList("--proxy-server=http://SESA357203:aladin@1@gateway.zscaler.net"));
		
			cap.setPlatform(org.openqa.selenium.Platform.ANY);
			
			//Proxy proxy = new Proxy();
			//proxy.setHttpProxy("http://gateway.zscaler.net");
			//cap.setCapability("proxy",proxy);
			//cap.setCapability(ChromeDriver.INTRODUCE_FLAKINESS_BY_IGNORING_SECURITY_DOMAINS,true);
			//cap.setCapability("chrome.binary","C:/Users/%USERNAME%/AppData/Local/Google/Chrome/Application/chrome.exe");
			//System.out.println("I m here Lokesh 44444 >>> ");
			//driver = new RemoteWebDriver(new URL("http://localhost:4444/wd/hub"),DesiredCapabilities.chrome());
			driver = new RemoteWebDriver (new URL("http://10.218.24.105:4444/wd/hub"),cap);
			//driver = new ChromeDriver(cap);
		//	System.out.println("I m here Lokesh 55555 >>> ");
			// ATUReports.setWebDriver(driver);
		//	driver = new ChromeDriver();
			backup_chrome = driver;
			
		}else
			System.out.println(browser +" Browser not supported!!!");
		
		//cap.setCapability("_important", "2");
		//if(driver==null)
		//driver = new RemoteWebDriver(new URL("http://localhost:4444/wd/hub"),cap);
		setAuthorInfoForReports();
		setIndexPageDescription();
		driver.manage().deleteAllCookies();
		System.out.println("Cookies deleted...");
		driver.manage().window().maximize();
		driver.manage().timeouts().implicitlyWait(10,TimeUnit.SECONDS);
		//driver.manage().timeouts().pageLoadTimeout(20, TimeUnit.SECONDS);
		//return "Pass";
	}catch(Throwable t){
		//System.out.println("Unable to open browser : " + browser);
		System.out.println("error is  >>  "+t);
		ErrorUtil.addVerificationFailure(t);
		//return "Fail";
	}
	}
	 private void setAuthorInfoForReports() {
		 System.out.println("setautherinfo done");
       //  ATUReports.setAuthorInfo("Naveen Lamba", Utils.getCurrentTime(),
            //          "1.0");
  }

  private void setIndexPageDescription() {
	  	 System.out.println("SetIndexPageDes done");
         //ATUReports.indexPageDescription = "WebMachine Automation <br/> <b>Test reports for WebMachine automation suite</b>";
  }
	
	public String fnNavigate(){
		// This will navigate to the URL
		driver.get("http://www.google.com");
		driver.manage().timeouts().implicitlyWait(40, TimeUnit.SECONDS);
		WebDriverWait wait = new WebDriverWait(driver, 40);
		//WebElement element = 
		wait.until(ExpectedConditions.titleContains("Google"));
		System.out.println("Base URL for test environment : "+ENV.getProperty("baseURL"));
		driver.get(ENV.getProperty("baseURL"));
		return "Pass";
	}
	
	
		
}








 