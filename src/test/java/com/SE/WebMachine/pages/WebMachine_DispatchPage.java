/*package com.SE.WebMachine.pages;
import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindAll;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.testng.Assert;
import org.testng.Reporter;
import com.SE.WebMachine.base.WebMachine_Base_Page;
import com.SE.WebMachine.util.Constants;
import com.SE.WebMachine.util.ErrorUtil;
public class WebMachine_DispatchPage extends WebMachine_Base_Page{

	//This will contain all the methods for validating functionality on WebMachine dispatch page.
	
				
			@FindBy(xpath = Constants.WebMachine_DispatchPage.Social_Options_Header_Link)
			public WebElement Social_Options_Header_Link;
			
			@FindBy(xpath = Constants.WebMachine_DispatchPage.FaceBook_Header_Link)
			public WebElement FaceBook_Header_Link;
			
			@FindBy(xpath = Constants.WebMachine_DispatchPage.MoreSocialMedia_Header_Link)
			public WebElement MoreSocialMedia_Header_Link;
			
			@FindBy(xpath = Constants.WebMachine_DispatchPage.OurCompnay_Header_Link)
			public WebElement OurCompnay_Header_Link;
			
			@FindBy(xpath = Constants.WebMachine_DispatchPage.SelectCountry_Header_Link)
			public WebElement SelectCountry_Header_Link;
			
			@FindBy(xpath = Constants.WebMachine_DispatchPage.Twitter_Header_Link)
			public WebElement Twitter_Header_Link;
			
			@FindBy(xpath = Constants.WebMachine_DispatchPage.ZoneA_Link)
			public WebElement ZoneA_Link;
			
			@FindBy(xpath = Constants.WebMachine_DispatchPage.ZoneB_Link)
			public WebElement ZoneB_Link;
			
			@FindBy(xpath = Constants.WebMachine_DispatchPage.ZoneC_Link)
			public WebElement ZoneC_Link;
			
			@FindBy(xpath = Constants.WebMachine_HomeLandingPage.LifeIsOn_Img)
			public WebElement LifeIsOn_Img;
			
			@FindBy(how = How.XPATH, using = Constants.WebMachine_DispatchPage.CountryName_Link)
			public WebElement CountryName_Link;
				
			@FindAll({@FindBy(how = How.XPATH, using = Constants.WebMachine_DispatchPage.ZoneSection_Link)})
			public List<WebElement> ZoneSection_Links;
			
			@FindBy(xpath = Constants.WebMachine_DispatchPage.searchInputBox)
			public WebElement searchInputBox;
			
			@FindBy(xpath = Constants.WebMachine_DispatchPage.searchSubmitButton)
			public WebElement searchSubmitButton;
			
			
			public WebDriver driver = null;
			
			public WebMachine_DispatchPage(WebDriver dr){      // This is constructor of dispatch page.
				
				driver = WebMachine_Base_Page.driver;
				
			}
			
			public void fnValidate_OurCompanyLink(){
				
				// This function will validate Our Company link available on header of dispatch page.
				
				try{
					
					Assert.assertTrue(fnisElementPresent(Constants.WebMachine_DispatchPage.OurCompnay_Header_Link), "Our Company link is Not found on dispatch page.");
					String strOurCompanyLink = OurCompnay_Header_Link.getText();
					String strBaseURL = driver.getCurrentUrl();
					fnClick(OurCompnay_Header_Link);
					fnSwitchToNewWindow();
					String strTitle = driver.getTitle();
					fnCheckCanonicalURL();
					boolean bolURL2 = strBaseURL.equalsIgnoreCase(driver.getCurrentUrl());
					if(!bolURL2){
					if( driver.getTitle().contains("Error") ||  driver.getTitle().contains("404")){
						
						try{
							Assert.fail("Error! Link "+ strOurCompanyLink+" is Broken");
							}catch(Throwable t){
								Reporter.log("Error! Link " +" : " +strOurCompanyLink+ " is Broken", false);
								fnLanguageConverter("Error! Link " +" : " +strOurCompanyLink+ " is Broken");
								//ATUReports.add("Error! Link " +" : " +strOurCompanyLink+ " is Broken", LogAs.FAILED, new CaptureScreen(ScreenshotOf.BROWSER_PAGE));
								ErrorUtil.addVerificationFailure(t);
								fnSwitchtoMainwindow();
							}
					}else{
						
						Reporter.log("Landing page Url : "+ driver.getCurrentUrl(), true);
						Reporter.log("Landing page Title : " + driver.getTitle(), false);
						fnLanguageConverter("Landing page Title : " + driver.getTitle());
						Reporter.log("Link " +" : " +strOurCompanyLink+ " is working fine ", false);
						fnLanguageConverter("Link " +" : " +strOurCompanyLink+ " is working fine ");
						//ATUReports.add("Link " +" : " +strOurCompanyLink+ " is working fine", LogAs.PASSED, new CaptureScreen(ScreenshotOf.BROWSER_PAGE));
						fnSwitchtoMainwindow();
						
						
					}
					}else {
						
						Assert.fail("Our Company link : " + strOurCompanyLink +" is Not working" );
					}
				}catch(Throwable t){
					Reporter.log("Error! Validation failed for Our Company ",true );
					ErrorUtil.addVerificationFailure(t);
					
				}
				
			}
	
			
			
			public void fnValidate_DispatchPageContent(){
				// This will validate different zones like @Home, @Work and Partners link functionality.
								
				try{
					// Find out number of zones available
					
					//List<WebElement> weZoneCount =  ZoneSection_Links ; //driver.findElements(By.xpath("//div[@id='diagonal-splash']/a"));
					Assert.assertTrue(ZoneSection_Links.size()!=0, "Error! No Zones found on Dispatch page.");
					Reporter.log("Total Zones available on Dispatch page for Country : " +CountryName_Link.getText()+" = "+ZoneSection_Links.size(), true);
					//ATUReports.add("Total Zones available on Dispatch page for Country : " +CountryName_Link.getText()+" = "+ZoneSection_Links.size(), LogAs.INFO, null);
					for(int i=1;i<=ZoneSection_Links.size();i++){
						try{
							Assert.assertEquals(fnisElementPresent(Constants.WebMachine_DispatchPage.ZoneSection_Link), true,"Error! NO Zone link found");
						System.out.println(driver.findElement(By.xpath(Constants.WebMachine_DispatchPage.ZoneSection_Link+"["+i+"]")).getAttribute("id")+ " :  "+driver.findElement(By.xpath(Constants.WebMachine_DispatchPage.ZoneSection_Link+"["+i+"]")).getAttribute("href"));
						}catch(Throwable e){
							
							ErrorUtil.addVerificationFailure(e);
						}
						
					}
				
					// Now click on each zone and validate its landing page.
					
					if(ZoneSection_Links.size()==3){
						
						Reporter.log("Total 3 zones are available on dispatch page for country : " + CountryName_Link.getText(), true);
						try{
							Reporter.log("Validating @Home Zone on dispatch page", true);
							Assert.assertTrue(fnValidate_ZoneA(), "@Home home page is Not available.");
							Reporter.log("@Home home page is available", true);
						   }catch(Throwable t){
							   ErrorUtil.addVerificationFailure(t);	
							   Reporter.log("Error! @Home home page is Not available", true);	
						   }
						
						try{
							Reporter.log("Validating @Work Zone on dispatch page", true);
							Assert.assertTrue(fnValidate_ZoneB(), "@Work home page is Not available.");
							Reporter.log("@Work home page is available", true);
							}catch(Throwable t){
							ErrorUtil.addVerificationFailure(t);	
							Reporter.log("Error! @Work home page is Not available", true);	
							//ATUReports.add("Error! @Work home page is Not available", LogAs.FAILED, new CaptureScreen(ScreenshotOf.BROWSER_PAGE));
							}
						
						try{
							Reporter.log("Validating Partners Zone on dispatch page", true);
							Assert.assertTrue(fnValidate_ZoneC(), "Partners home page is Not available.");
							Reporter.log("Partners home page is available", true);
							}catch(Throwable t){
							ErrorUtil.addVerificationFailure(t);	
							Reporter.log("Error! Partners home page is Not available", true);	
							//ATUReports.add("Error! Partners home page is Not available", LogAs.FAILED, new CaptureScreen(ScreenshotOf.BROWSER_PAGE));
							}
						
						
					}if(ZoneSection_Links.size()==2){
						
						try{
							Reporter.log("Validating @Work Zone on dispatch page", true);
							Assert.assertTrue(fnValidate_ZoneA(), "@Work home page is Not available.");
							Reporter.log("@Work home page is available", true);
						   }catch(Throwable t){
							   ErrorUtil.addVerificationFailure(t);	
							   Reporter.log("Error! @Work home page is Not available", true);	
							   //ATUReports.add("Error! @Work home page is Not available", LogAs.FAILED, new CaptureScreen(ScreenshotOf.BROWSER_PAGE));
						   }
						
						try{
							Reporter.log("Validating Partners Zone on dispatch page", true);
							Assert.assertTrue(fnValidate_ZoneB(), "Partners home page is Not available.");
							Reporter.log("Partners home page is available", true);
							//ATUReports.add("Partners home page is available", LogAs.PASSED, new CaptureScreen(ScreenshotOf.BROWSER_PAGE));
							}catch(Throwable t){
							ErrorUtil.addVerificationFailure(t);	
							Reporter.log("Error! Partners home page is Not available", true);	
							//ATUReports.add("Error! Partners home page is Not available", LogAs.FAILED, new CaptureScreen(ScreenshotOf.BROWSER_PAGE));
							}
						
					}
					
					
				}catch(Throwable t){
					Reporter.log("Error! No Zones found on Dispatch page.", true);
					//ATUReports.add("Error! No Zones found on Dispatch page", LogAs.FAILED, new CaptureScreen(ScreenshotOf.BROWSER_PAGE));
					ErrorUtil.addVerificationFailure(t);
				}
				
				
				
				
				
				
			}
			
		
			
			public boolean fnValidate_ZoneA(){
				
			//  This function validates if clicking @Home zone on Dispatch page navigates user to @Home home page.
				
				try{
				//	Reporter.log("Validating @Home zone on Dispatch page...", true);
					fnClick(ZoneA_Link);
					String StrCurrentURL = driver.getCurrentUrl();
					Reporter.log("Landing page URL : "+StrCurrentURL, true);
					if(driver.getTitle().contains("Error") || driver.getTitle().contains("404")){
						//Assert.fail("Zone A home page NOT displayed successfully");
						return false;
					}
					//Reporter.log("Zone A home page is Displayed successfully", true);
					fnCheckCanonicalURL();
					driver.navigate().back();
					return true;
				}catch(Throwable t){
					driver.get(ENV.getProperty("baseURL"));
					ErrorUtil.addVerificationFailure(t);
					//Reporter.log("ZoneA_Link Not wroking ", true);
					return false;
					
				}
				
			}
			
			public boolean fnValidate_ZoneB(){
				
			//  This function validates if clicking @Work zone on Dispatch page navigates user to @Work home page.	
				
				try{
					//Reporter.log("Validating @Work zone on Dispatch page...", true);
					ZoneB_Link.click();
					String StrCurrentURL = driver.getCurrentUrl();
					Reporter.log("Landing page URL : "+StrCurrentURL, true);
					if(driver.getTitle().contains("Error") || driver.getTitle().contains("Error")){
						//Assert.fail("@Work home page NOT displayed successfully");
						return false;
						
					}
				//	Reporter.log("Zone B home page is Displayed successfully", true);
					fnCheckCanonicalURL();
					driver.navigate().back();
					return true;
					
				}catch(Throwable t){
					driver.get(ENV.getProperty("baseURL"));
					ErrorUtil.addVerificationFailure(t);
					Reporter.log("ZoneB_Link Not working ", true);
					return false;
				}
				
			}
			
			public boolean fnValidate_ZoneC(){
			
			//  This function validates if clicking Partners zone on Dispatch page navigates user to @Work home page.
				
	            try{		
	            	//Reporter.log("Validating zone C on Dispatch page...", true);
	            	ZoneC_Link.click();
					String StrCurrentURL = driver.getCurrentUrl();
					Reporter.log("Landing page URL : "+StrCurrentURL, true);
					if(driver.getTitle().contains("Error") || driver.getTitle().contains("Error")){
						//Assert.fail("Partners home page NOT displayed successfully");
						return false;
					}
					
					//Reporter.log("Partners home page is Displayed successfully", true);
					fnCheckCanonicalURL();
					driver.navigate().back();
					return true;
					
				}catch(Throwable t){
					driver.get(ENV.getProperty("baseURL"));
					ErrorUtil.addVerificationFailure(t);
				//	Reporter.log("PartnersZoneC_Link Not working ", true);
					return false;
					
				}
			}
			
			
}







*/