package com.SE.WebMachine.pages;

import java.util.List;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindAll;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.testng.Reporter;



import com.SE.WebMachine.base.WebMachine_Base_Page;
import com.SE.WebMachine.util.Constants;
import com.SE.WebMachine.util.ErrorUtil;

public class WebMachine_SearchPage extends WebMachine_Base_Page{

public WebDriver driver = null;
	
@FindBy(how = How.XPATH, using = Constants.WebMachine_SearchPage.searchInputBox)
public WebElement searchInputBox;

@FindBy(how = How.CSS, using = Constants.WebMachine_SearchPage.searchSubmitButton)
public WebElement searchSubmitButton;

@FindAll({@FindBy(how = How.XPATH, using = Constants.WebMachine_SearchPage.guidedSearch)})
public List<WebElement> guidedSearch;

@FindAll({@FindBy(how = How.XPATH, using = Constants.WebMachine_SearchPage.searchInResultCheckbox)})
public List<WebElement> searchInResultCheckbox;

@FindBy(how = How.CSS, using = Constants.WebMachine_SearchPage.categoryDropdownFilter)
public WebElement categoryDropdownFilter;

@FindBy(how = How.XPATH, using = Constants.WebMachine_SearchPage.sortByUpdatedLink)
public WebElement sortByUpdatedLink;

@FindBy(how = How.XPATH, using = Constants.WebMachine_SearchPage.searchByDateRelevanceDropdownFilter)
public WebElement searchByDateRelevanceDropdownFilter;

	public WebMachine_SearchPage(WebDriver dr){      // This is constructor of BeInspired page.
		
		driver = WebMachine_Base_Page.driver;
		
	}

public void fnValidate_GuidedSearch(){
	// This will validate BeInspired Landing page in @Home section.
	
	Reporter.log("Validating BeInspired page", true);
	try{		
		sendKeys(Constants.WebMachine_SearchPage.searchInputBox, "art", "@Home search input box not found on home page");		
		reporterLogOnly("Going to click gudied search result ", true);
		//List<WebElement> guidedResult =  getWebElementsAllIsExist(Constants.WebMachine_SearchPage.guidedSearch, "@Home guided search result not found");
		//guidedResult.get(0).click();
		getWebElementsAllIsExistClick(Constants.WebMachine_SearchPage.guidedSearch, 0, "@Home guided search result not found", "Going to select gudied search result", true, "Error! guided search result not available", false);
		ClickAtuLog(Constants.WebMachine_SearchPage.searchSubmitButton, "@Home search submit button not found", "Going to click submit button", true, "Erro! Search submit button not available.", false);
		}
		catch(Throwable t){
			reporterLog("Error! fnValidate_GuidedSearch failed", true, "Error! @Home guided search failed", false);		
			ErrorUtil.addVerificationFailure(t);
	}
	
	System.err.println("Test fnValidate finished");
	
	
}
	

	
}
