package com.SE.WebMachine.pages;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Date;
import java.util.List;

import net.sourceforge.htmlunit.corejs.javascript.ast.DoLoop;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
//import org.openqa.selenium.internal.seleniumemulation.IsElementPresent;
import org.openqa.selenium.support.FindAll;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.testng.Assert;
import org.testng.Reporter;

import org.openqa.selenium.support.ui.WebDriverWait;
import java.util.concurrent.TimeUnit;



import bsh.ParseException;

import com.SE.WebMachine.base.WebMachine_Base_Page;
import com.SE.WebMachine.util.Constants;
import com.SE.WebMachine.util.ErrorUtil;

public class WebMachine_BeInspiredPage extends WebMachine_Base_Page{

public WebDriver driver = null;
	
@FindBy(how = How.XPATH, using = Constants.WebMachine_BeInspiredPage.BeInspiredTab_Link)
public WebElement BeInspiredTab_Link;

@FindBy(how = How.CSS, using = Constants.WebMachine_BeInspiredPage.BeInspiredPageTitle)
public WebElement BeInspiredPageTitle;

@FindAll({@FindBy(how = How.XPATH, using = Constants.WebMachine_BeInspiredPage.BeInspiredTiles)})
public List<WebElement> BeInspiredTiles;

@FindAll({@FindBy(how = How.XPATH, using = Constants.WebMachine_BeInspiredPage.DemoRoomTiles)})
public List<WebElement> DemoRoomTiles;

@FindBy(how = How.CSS, using = Constants.WebMachine_BeInspiredPage.DemoRoomPageTitle)
public WebElement DemoRoomPageTitle;

@FindBy(how = How.XPATH, using = Constants.WebMachine_BeInspiredPage.DemoRoom_Link)
public WebElement DemoRoom_Link;

@FindBy(how = How.XPATH, using = Constants.WebMachine_BeInspiredPage.DemoRoomName)
public WebElement DemoRoomName;





@FindBy(how = How.XPATH, using = Constants.WebMachine_SearchPage.searchInputBox)
public WebElement searchInputBox;

@FindBy(how = How.CSS, using = Constants.WebMachine_SearchPage.searchSubmitButton)
public WebElement searchSubmitButton;

@FindAll({@FindBy(how = How.XPATH, using = Constants.WebMachine_SearchPage.guidedSearch)})
public List<WebElement> guidedSearch;

@FindAll({@FindBy(how = How.XPATH, using = Constants.WebMachine_SearchPage.searchInResultCheckbox)})
public List<WebElement> searchInResultCheckbox;

@FindBy(how = How.CSS, using = Constants.WebMachine_SearchPage.categoryDropdownFilter)
public WebElement categoryDropdownFilter;

@FindBy(how = How.XPATH, using = Constants.WebMachine_SearchPage.sortByUpdatedLink)
public WebElement sortByUpdatedLink;

@FindBy(how = How.XPATH, using = Constants.WebMachine_SearchPage.searchByDateRelevanceDropdownFilter)
public WebElement searchByDateRelevanceDropdownFilter;


	public WebMachine_BeInspiredPage(WebDriver dr){      // This is constructor of BeInspired page.
		
		driver = WebMachine_Base_Page.driver;
		
	}

	
	public void fnValidate_GuidedSearch() throws ParseException, InterruptedException, java.text.ParseException{
		// This will validate BeInspired Landing page in @Home section.
		
		Reporter.log("Validating Search page", true);
		try{
			//driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
			//driver.navigate().refresh();
			//driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
			//getWebElementsAllIsExistClick("//a[@class='search-button']", 0, "@Home guided search result not found", "Select guided search result", true, "Error! guided search result not available", true);
			driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
			sendKeys(Constants.WebMachine_SearchPage.searchInputBox, "rel", "@Home search input box not found on home page");		
			driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
			reporterLogOnly("Going to click Guided search result ", true);
			getWebElementsAllIsExistClick(Constants.WebMachine_SearchPage.guidedSearch, 0, "@Home guided search result not found", "Select gudied search result", true, "Error! guided search result not available", true);
			//ClickAtuLog(Constants.WebMachine_SearchPage.searchSubmitButton, "@Home search submit button not found", "Going to click submit button", true, "Erro! Search submit button not available.", true);
			isElementExist(Constants.WebMachine_SearchPage.searchByDateRelevanceDropdownFilter, "@SearchResult searchByDate dropdown not found");
			isElementExist(Constants.WebMachine_SearchPage.searchInResultCheckbox, "@SearchResult searchInResultCheckbox not found");
			reporterLogOnly("Going to click search by date from dropdown ", true);
			selectDropdownElementByVisibleText(Constants.WebMachine_SearchPage.searchByDateRelevanceDropdownFilter, "date", "@SearchResult searchByDateRelevanceDropdownFilter not found");			
			List<WebElement> allLinks = getWebElementsAllIsExist(Constants.WebMachine_SearchPage.sortByUpdatedLink, "UpdatedLink in search result not found");
		    System.out.println("Count of updated by Links-->"+allLinks.size());
			ClickAtuLog(Constants.WebMachine_SearchPage.searchInResultCheckbox, "@SearchResult searchInResultCheckbox not found", "Going to click searchInResultCheckbox", true, "Error! searchInResultCheckbox not found", true);
			ClickAtuLog(Constants.WebMachine_SearchPage.searchSubmitButton, "@Home search submit button not found", "Going to click submit button", true, "Erro! Search submit button not available.", true);
			isElementExist(Constants.WebMachine_SearchPage.searchByDateRelevanceDropdownFilter, "@SearchResult searchByDate dropdown not found");
			isElementExist(Constants.WebMachine_SearchPage.searchInResultCheckbox, "@SearchResult searchInResultCheckbox not found");
		    
		}
		
			catch(Throwable t){
				System.out.println("Test ends here");
				//reporterLog("Error! fnValidate_GuidedSearch failed", true, "Error! @Home guided search failed", true);		
				//ErrorUtil.addVerificationFailure(t);
		}		
		System.err.println("Test fnValidate finished");
		
		driver.quit();
	}

	
	
	
	
public void fnValidate_BeInspiredLandingPage(){
	// This will validate BeInspired Landing page in @Home section.
	
	Reporter.log("Validating BeInspired page", true);
	try{
		
		Assert.assertTrue(fnisElementPresent(Constants.WebMachine_DispatchPage.ZoneA_Link), "@Home Section Not found on dispatch page.");
		fnClick(ZoneA_Link);
		Assert.assertTrue(fnisElementPresent(Constants.WebMachine_BeInspiredPage.BeInspiredTab_Link), "BeInspired tab Not found on horizontal navigation pane.");
		String strBaseURL = driver.getCurrentUrl();
		Reporter.log("Going to click on BeInspired tab on horizontal navigation pane " , true);
		fnClick(BeInspiredTab_Link);
		fnCheckCanonicalURL();
		boolean bolURL = strBaseURL.equalsIgnoreCase(driver.getCurrentUrl());
		//System.out.println("bolURL" + bolURL);
		if(!bolURL){
			if( driver.getTitle().contains("Error") ||  driver.getTitle().contains("404")){
				
				try{
					Assert.fail("Error! BeInspired Link is Broken");
					}catch(Throwable t){
						////ATUReports.add("Erro! BeInspired landing page NOT available.", LogAs.FAILED, new CaptureScreen(ScreenshotOf.BROWSER_PAGE));
						Reporter.log("Error! BeInspired Link is Broken", true);
						ErrorUtil.addVerificationFailure(t);
						driver.navigate().back();
					}
			}else{
				
				
				try{
					////ATUReports.add("BeInspired landing page is available.", LogAs.PASSED, new CaptureScreen(ScreenshotOf.BROWSER_PAGE));
					Reporter.log("BeInspired tab link is working fine", true);
					Reporter.log("Title of landing page : " +driver.getTitle(), false);
					fnLanguageConverter("Title of landing page : " +driver.getTitle());
					Reporter.log("Header of page : "+ BeInspiredPageTitle.getText(), false);
					fnLanguageConverter("Header of page : "+ BeInspiredPageTitle.getText());
					Reporter.log("Number of demo rooms available : "+ BeInspiredTiles.size(), false);
					fnLanguageConverter("Number of product catogories available : "+ BeInspiredTiles.size());
					for(int i=1; i<=BeInspiredTiles.size();i++){
						
						String strDemoRoomName = driver.findElement(By.xpath(Constants.WebMachine_BeInspiredPage.BeInspiredTiles+"["+i+"]"+"/a/div/span[2]/em")).getText();
						Reporter.log("Demo Room Name : "+i+" : "+strDemoRoomName , false);
						fnLanguageConverter("Demo Room Name : "+i+" : "+strDemoRoomName );
						
						
					}
					
					
				}catch(Throwable t){
					Reporter.log("Error!BeInspired Link is Broken", true);
					////ATUReports.add("Error! BeInspired landing page Not found", LogAs.FAILED, new CaptureScreen(ScreenshotOf.BROWSER_PAGE));
					ErrorUtil.addVerificationFailure(t);
					
				}
			
		}
		
	}
	}catch(Throwable t){
		////ATUReports.add("Error! BeInspired landing page Validation failed", LogAs.FAILED, new CaptureScreen(ScreenshotOf.BROWSER_PAGE));
		Reporter.log("Error! Validation failed for BeInpsired Page", true);
		ErrorUtil.addVerificationFailure(t);
	}
	
}
	

public void fnValidateDemoRoom(){
	
	// This will validate first demo room in BeInspired page room types.
	
	Reporter.log("Validating first demo room on BeInspired page", true);
	
try{
		
		Assert.assertTrue(fnisElementPresent(Constants.WebMachine_BeInspiredPage.BeInspiredTab_Link), "BeInspired tab Not found on horizontal navigation pane.");
		String strBaseURL = driver.getCurrentUrl();
		fnClick(BeInspiredTab_Link);
		Assert.assertTrue(fnisElementPresent(Constants.WebMachine_BeInspiredPage.DemoRoom_Link), "Error! demo room link Not found");
		String strDemoRoomName = DemoRoomName.getText();
		////ATUReports.add("First demo room available on BeInspired page as : "+ strDemoRoomName, LogAs.PASSED, new CaptureScreen(ScreenshotOf.BROWSER_PAGE));
		fnClick(DemoRoom_Link);
		//fnCheckCanonicalURL();
		boolean bolURL = strBaseURL.equalsIgnoreCase(driver.getCurrentUrl());
		//System.out.println("bolURL" + bolURL);
		if(!bolURL){
			if( driver.getTitle().contains("Error") ||  driver.getTitle().contains("404")){
				
				try{
					Assert.fail("Error! "+strDemoRoomName+ " link is Broken");
					}catch(Throwable t){
						Reporter.log("Error! Demo room "+strDemoRoomName+ " link is Broken", true);
						////ATUReports.add("Error! Demo room "+strDemoRoomName+ " link is Broken", LogAs.FAILED, new CaptureScreen(ScreenshotOf.BROWSER_PAGE));
						ErrorUtil.addVerificationFailure(t);
						driver.navigate().back();
					}
			}else{
				
				
				try{
					
					Reporter.log("Title of landing page : " +driver.getTitle(), false);
					fnLanguageConverter("Title of landing page : " +driver.getTitle());
					Reporter.log("Header of page : "+ DemoRoomPageTitle.getText(), false);
					fnLanguageConverter("Header of page : "+ DemoRoomPageTitle.getText());
					Reporter.log("Number SE products available for living room : "+ DemoRoomTiles.size(), false);
					fnLanguageConverter("Number of SE products available for living room : "+ DemoRoomTiles.size());
					////ATUReports.add(strDemoRoomName+" landing page is available", LogAs.PASSED, new CaptureScreen(ScreenshotOf.BROWSER_PAGE));
					for(int i=1; i<=DemoRoomTiles.size();i++){
						
						String strDemoRoomOptions = driver.findElement(By.xpath(Constants.WebMachine_BeInspiredPage.DemoRoomTiles+"["+i+"]"+"/a/div/span[2]/em")).getText();
						Reporter.log(strDemoRoomName+" SE product options : "+i+" : "+strDemoRoomOptions , false);
						fnLanguageConverter(strDemoRoomName+" SE product options : "+i+" : "+strDemoRoomOptions );
						
						
					}
					
					
				}catch(Throwable t){
					Reporter.log("Error! "+strDemoRoomName+" link is Broken", true);
					////ATUReports.add("Error! "+strDemoRoomName+" landing page Not found", LogAs.FAILED, new CaptureScreen(ScreenshotOf.BROWSER_PAGE));
					ErrorUtil.addVerificationFailure(t);
					
				}
			
		}
		
	}
	}catch(Throwable t){
		
		Reporter.log("Error! Validation failed for Demo room Page", true);
		////ATUReports.add("Error! Demo room validation failed", LogAs.FAILED, new CaptureScreen(ScreenshotOf.BROWSER_PAGE));
		ErrorUtil.addVerificationFailure(t);
	}
	
	
	
}
	
}
