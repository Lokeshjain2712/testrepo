package com.SE.WebMachine.pages;

import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindAll;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.testng.Assert;
import org.testng.Reporter;

import com.SE.WebMachine.base.WebMachine_Base_Page;
import com.SE.WebMachine.util.Constants;
import com.SE.WebMachine.util.ErrorUtil;

public class WebMachine_YourProjectPage extends WebMachine_Base_Page{

	// This class will have functions to validate your project tab on @Home horizontal navigation pane.
	
@FindBy(how = How.XPATH, using = Constants.WebMachine_YourProjectPage.YourProjectTab_Link)
public WebElement YourProjectTab_Link;

@FindBy(how = How.CSS, using = Constants.WebMachine_YourProjectPage.YourProjectDemoTilePageTitle)
public WebElement YourProjectDemoTilePageTitle;

@FindAll({@FindBy(how = How.XPATH, using = Constants.WebMachine_YourProjectPage.YourProjectTiles)})
public List<WebElement> YourProjectTiles;

@FindBy(how = How.CSS, using = Constants.WebMachine_YourProjectPage.YourProjectPageTitle)
public WebElement YourProjectPageTitle;


@FindBy(how = How.CSS, using = Constants.WebMachine_YourProjectPage.YourProjectDemoTile_Link)
public WebElement YourProjectDemoTile_Link;

@FindBy(how = How.XPATH, using = Constants.WebMachine_YourProjectPage.YourProjectDemoTileName)
public WebElement YourProjectDemoTileName;


public WebMachine_YourProjectPage(WebDriver dr){      // This is constructor of Your Project page.
		
		driver = WebMachine_Base_Page.driver;
		
}


public void fnValidate_YourProjectPage(){
		
		// This function will validate your project's landing page
		
	Reporter.log("Validating Your Project page", true);
	try{
		
		Assert.assertTrue(fnisElementPresent(Constants.WebMachine_DispatchPage.ZoneA_Link), "@Home Section Not found on dispatch page.");
		fnClick(ZoneA_Link);
		Assert.assertTrue(fnisElementPresent(Constants.WebMachine_YourProjectPage.YourProjectTab_Link), "Your Project tab Not found on horizontal navigation pane.");
		String strBaseURL = driver.getCurrentUrl();
		Reporter.log("Going to click on Your Project tab on horizontal navigation pane " , true);
		fnClick(YourProjectTab_Link);
		fnCheckCanonicalURL();
		boolean bolURL = strBaseURL.equalsIgnoreCase(driver.getCurrentUrl());
		//System.out.println("bolURL" + bolURL);
		if(!bolURL){
			if( driver.getTitle().contains("Error") ||  driver.getTitle().contains("404")){
				
				try{
					Assert.fail("Error! Your Project Link is Broken");
					}catch(Throwable t){
						//ATUReports.add("Erro! Your Project landing page NOT available.", LogAs.FAILED, new CaptureScreen(ScreenshotOf.BROWSER_PAGE));
						Reporter.log("Error! Your Project Link is Broken", true);
						ErrorUtil.addVerificationFailure(t);
						driver.navigate().back();
					}
			}else{
				
				
				try{
					//ATUReports.add("Your Project landing page is available.", LogAs.PASSED, new CaptureScreen(ScreenshotOf.BROWSER_PAGE));
					Reporter.log("Your Project tab link is working fine", true);
					Reporter.log("Title of landing page : " +driver.getTitle(), false);
					fnLanguageConverter("Title of landing page : " +driver.getTitle());
					Reporter.log("Header of page : "+ YourProjectPageTitle.getText(), false);
					fnLanguageConverter("Header of page : "+ YourProjectPageTitle.getText());
					Reporter.log("Number Project options available : "+ YourProjectTiles.size(), false);
					fnLanguageConverter("Number of Project catogories available : "+ YourProjectTiles.size());
					for(int i=1; i<=YourProjectTiles.size();i++){
						
						String strProjectName = driver.findElement(By.xpath(Constants.WebMachine_YourProjectPage.YourProjectDemoTileName+"["+i+"]"+"/a/div/span[2]/em")).getText();
						Reporter.log("Your Project Name : "+i+" : "+strProjectName , false);
						fnLanguageConverter("Your Project Name : "+i+" : "+strProjectName );
						
						
					}
					
					
				}catch(Throwable t){
					Reporter.log("Error! Your Project Link is Broken", true);
					//ATUReports.add("Error! Your Project landing page Not found", LogAs.FAILED, new CaptureScreen(ScreenshotOf.BROWSER_PAGE));
					ErrorUtil.addVerificationFailure(t);
					
				}
			
		}
		
	}else{
		
		try{
			Assert.fail("Error! Your Project Link is Not clickable");
			}catch(Throwable t){
				//ATUReports.add("Error! Your Project tab not clickable.", LogAs.FAILED, new CaptureScreen(ScreenshotOf.BROWSER_PAGE));
				Reporter.log("Error! Your Project tab not clickable", true);
				ErrorUtil.addVerificationFailure(t);
				//driver.navigate().back();
			}
	}
	}catch(Throwable t){
		//ATUReports.add("Error! Your Project landing page Validation failed", LogAs.FAILED, new CaptureScreen(ScreenshotOf.BROWSER_PAGE));
		Reporter.log("Error! Validation failed for Your Project Page", true);
		ErrorUtil.addVerificationFailure(t);
	}
			
	}
	

public void fnValidate_DemoProjects(){
	// This will validate demo projects pages of Your project options.
	
	Reporter.log("Validating first demo project of Your Project page options", true);
	
	try{
			
		Assert.assertTrue(fnisElementPresent(Constants.WebMachine_YourProjectPage.YourProjectTab_Link), "Your Project tab Not found on horizontal navigation pane.");
		String strBaseURL = driver.getCurrentUrl();
		//Reporter.log("Going to click on Your Project tab on horizontal navigation pane " , true);
		fnClick(YourProjectTab_Link);
		fnCheckCanonicalURL();
		//boolean bolURL = strBaseURL.equalsIgnoreCase(driver.getCurrentUrl());
		boolean bolURL = false;
		//System.out.println("bolURL" + bolURL);
		if(!bolURL){
			if( driver.getTitle().contains("Error") ||  driver.getTitle().contains("404")){
				
				try{
					Assert.fail("Error! Your Project Link is Broken");
					}catch(Throwable t){
						//ATUReports.add("Error! Your Project landing page NOT available.", LogAs.FAILED, new CaptureScreen(ScreenshotOf.BROWSER_PAGE));
						Reporter.log("Error! Your Project Link is Broken", true);
						ErrorUtil.addVerificationFailure(t);
						driver.navigate().back();
					}
			}else{
				
					
					try{
						
						
							
							String strProjectName = driver.findElement(By.xpath(Constants.WebMachine_YourProjectPage.YourProjectDemoTileName+"["+1+"]"+"/a/div/span[2]/em")).getText();
							Reporter.log("Validating Project : "+strProjectName , false);
							fnLanguageConverter("Validating project  : "+strProjectName );
							String strBaseURL2 = driver.getCurrentUrl();
							driver.findElement(By.xpath(Constants.WebMachine_YourProjectPage.YourProjectDemoTileName)).click();
							if( driver.getTitle().contains("Error") ||  driver.getTitle().contains("404")){
								
								try{
									Assert.fail("Error! "+strProjectName+" Link is Broken");
									}catch(Throwable t){
										//ATUReports.add("Error! "+strProjectName+" Link is Broken", LogAs.FAILED, new CaptureScreen(ScreenshotOf.BROWSER_PAGE));
										Reporter.log("Error! Your Project Link is Broken", true);
										ErrorUtil.addVerificationFailure(t);
										driver.navigate().back();
									}
							}else{
								Reporter.log(strProjectName+" Link is working fine", true);
								//ATUReports.add(strProjectName+" Link is working fine", LogAs.PASSED, new CaptureScreen(ScreenshotOf.BROWSER_PAGE));
								fnCheckCanonicalURL();
								driver.navigate().back();
							}
							
						}catch(Throwable t){
						Reporter.log("Error! Your Project Link is Broken", true);
						//ATUReports.add("Error! Your Project landing page Not found", LogAs.FAILED, new CaptureScreen(ScreenshotOf.BROWSER_PAGE));
						ErrorUtil.addVerificationFailure(t);
						
					}
				
			}
			
		}else{
			
			System.out.println("fucker in else again");
		}
		}catch(Throwable t){
			
			Reporter.log("Error! Validation failed for Your Project Page", true);
			//ATUReports.add("Error! Your Project validation failed", LogAs.FAILED, new CaptureScreen(ScreenshotOf.BROWSER_PAGE));
			ErrorUtil.addVerificationFailure(t);
		}
		
		
		
	
}	
	
	
}


