package com.SE.WebMachine.pages;

import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindAll;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.testng.Assert;
import org.testng.Reporter;

import com.SE.WebMachine.base.WebMachine_Base_Page;
import com.SE.WebMachine.util.Constants;
import com.SE.WebMachine.util.ErrorUtil;

public class WebMachine_ProductsPage extends WebMachine_Base_Page{

@FindBy(how = How.XPATH, using = Constants.WebMachine_ProductsPage.ProductsTab_Link)
public WebElement ProductsTab_Link;
	

@FindBy(how = How.CSS, using = Constants.WebMachine_ProductsPage.ProductsPageHeader)
public WebElement ProductsPageHeader;

@FindAll({@FindBy(how = How.CSS, using = Constants.WebMachine_ProductsPage.TotalProductCategories)})
public List<WebElement> TotalProductCategories;

@FindBy(how = How.XPATH, using = Constants.WebMachine_ProductsPage.ProductCategoryName)
public WebElement ProductCategoryName;

@FindBy(how = How.XPATH, using = Constants.WebMachine_ProductsPage.WhereToBuy_Link)
public WebElement WhereToBuy_Link;

public WebDriver driver = null;
	
	public WebMachine_ProductsPage(WebDriver dr){      // This is constructor of Products page.
		
		driver = WebMachine_Base_Page.driver;
		
	}
	

public void fnValidate_ProductsPage(){
	
	// This function will validate Products page contents
	
	Reporter.log("Validating Products page..", true);
	try{
		Assert.assertTrue(fnisElementPresent(Constants.WebMachine_DispatchPage.ZoneA_Link), "@Home Section Not found on dispatch page.");
		fnClick(ZoneA_Link);
		Assert.assertTrue(fnisElementPresent(Constants.WebMachine_ProductsPage.ProductsTab_Link), "Products tab Not found on horizontal navigation pane.");
		String strBaseURL = driver.getCurrentUrl();
		Reporter.log("Going to click on Products tab on horizontal navigation pane " , true);
		fnClick(ProductsTab_Link);
		fnCheckCanonicalURL();
		boolean bolURL = strBaseURL.equalsIgnoreCase(driver.getCurrentUrl());
		//System.out.println("bolURL" + bolURL);
		if(!bolURL){
			if( driver.getTitle().contains("Error") ||  driver.getTitle().contains("404")){
				
				try{
					Assert.fail("Error! Link is Broken");
					}catch(Throwable t){
						Reporter.log("Error! Link " +" :  is Broken", true);
						ErrorUtil.addVerificationFailure(t);
						driver.navigate().back();
					}
			}else{
				
				
				try{
					
					Reporter.log("Products tab link is working fine", true);
					Reporter.log("Title of landing page : " +driver.getTitle(), false);
					fnLanguageConverter("Title of landing page : " +driver.getTitle());
					Reporter.log("Header of page : "+ ProductsPageHeader.getText(), false);
					//ATUReports.add("Products landing page is available.", LogAs.PASSED, new CaptureScreen(ScreenshotOf.BROWSER_PAGE));
					fnLanguageConverter("Header of page : "+ ProductsPageHeader.getText());
					Reporter.log("Number of product catogories available : "+ TotalProductCategories.size(), false);
					fnLanguageConverter("Number of product catogories available : "+ TotalProductCategories.size());
					for(int i=1; i<=TotalProductCategories.size();i++){
						
						String strProductCategoryName = driver.findElement(By.xpath(Constants.WebMachine_ProductsPage.ProductCategoryName+"["+i+"]"+"/h2/a")).getText();
						Reporter.log("Product category : "+i+" : "+strProductCategoryName , false);
						fnLanguageConverter("Product category : "+i+" : "+strProductCategoryName );
						
						
					}
					
					// Now click on each product category and validate its landing page
					try{
						
						for(int i=1; i<=TotalProductCategories.size();i++){
							String strProductCategoryName = driver.findElement(By.xpath(Constants.WebMachine_ProductsPage.ProductCategoryName+"["+i+"]"+"/h2/a")).getText();
							Reporter.log("Validating Product category "+i+" : "+strProductCategoryName, false);
							fnLanguageConverter("Validating Product category "+i+" : "+strProductCategoryName);
							String strbaseUrl = driver.getCurrentUrl();
							driver.findElement(By.xpath(Constants.WebMachine_ProductsPage.ProductCategoryName+"["+i+"]"+"/h2/a")).click();
							boolean bolProductCategoryPageURL = strbaseUrl.equalsIgnoreCase(driver.getCurrentUrl());
							//System.out.println("bolProductCategoryPageURL" + bolProductCategoryPageURL);
							if(!bolURL){
								if( driver.getTitle().contains("Error") ||  driver.getTitle().contains("404")){
									
									try{
										Assert.fail("Error! Category link is Broken");
										}catch(Throwable t){
											Reporter.log("Error! Category Link is Broken", true);
											ErrorUtil.addVerificationFailure(t);
											driver.navigate().back();
										}
								}else{
									
									Reporter.log("Product category : "+strProductCategoryName +" is working fine", false);
									fnLanguageConverter("Product category : "+strProductCategoryName +" is working fine");
									//ATUReports.add("Product category : "+ strProductCategoryName+" is working fine", LogAs.PASSED, new CaptureScreen(ScreenshotOf.BROWSER_PAGE));
									Reporter.log("Title of category page : "+driver.getTitle(), false);
									fnLanguageConverter("Title of category page : "+driver.getTitle());
									fnCheckCanonicalURL();
									driver.navigate().back();
								}
							}
							
							
						}
						
						
					}catch(Throwable e){
						
						Reporter.log("Error in validating Product category", true);
						ErrorUtil.addVerificationFailure(e);
					}
					//driver.navigate().back();
				}catch(Throwable t){
					Reporter.log("Products tab link is NOT working", true);
					ErrorUtil.addVerificationFailure(t);
					
				}
				
			}
		
		
		}
	}catch(Throwable t){
		
		Reporter.log("Error! Products tab validation failed.", true);
		ErrorUtil.addVerificationFailure(t);
	}
	
	
}

public void fnValidate_WheretoBuyLink(){
	// This will validate where to buy link on each product landing page.
	
	try{
		Assert.assertTrue(fnisElementPresent(Constants.WebMachine_ProductsPage.WhereToBuy_Link), "Error! Where to buy link Not found");
		//ATUReports.add("Where to buy link is available", LogAs.PASSED, new CaptureScreen(ScreenshotOf.BROWSER_PAGE));
		Reporter.log("Where to buy link is available with link text : "+ WhereToBuy_Link.getText(), false);
		fnLanguageConverter("Where to buy link is available with link text : "+ WhereToBuy_Link.getText());
		Reporter.log("Where to buy link href : "+ WhereToBuy_Link.getAttribute("href"), false);
		fnLanguageConverter("Where to buy link href : "+ WhereToBuy_Link.getAttribute("href"));
		
	}catch(Exception e){
		//ATUReports.add("Error! Where to buy link Not found.", LogAs.FAILED, new CaptureScreen(ScreenshotOf.BROWSER_PAGE));
		Reporter.log("Error! Where to buy link Not found.", true);
		ErrorUtil.addVerificationFailure(e);
	}
	
	
}
	
public void fnValidate_ProductCategory(){
	
	// This will validate Product in its respective category.
	
	try{
		
		Assert.assertTrue(fnisElementPresent(Constants.WebMachine_ProductsPage.ProductsTab_Link), "Products tab Not found on horizontal navigation pane.");
		//String strBaseURL = driver.getCurrentUrl();
		Reporter.log("Going to validate first product available in all categories. " , true);
		fnClick(ProductsTab_Link);
		fnCheckCanonicalURL();
		boolean bolURL =  false;    // for testing purpose ---> strBaseURL.equalsIgnoreCase(driver.getCurrentUrl());
		//System.out.println("bolURL" + bolURL);
		if(!bolURL){
			if( driver.getTitle().contains("Error") ||  driver.getTitle().contains("404")){
				
				try{
					Assert.fail("Error! Link is Broken");
					}catch(Throwable t){
						Reporter.log("Error! Link " +"is Broken", true);
						ErrorUtil.addVerificationFailure(t);
						driver.navigate().back();
					}
			}else{
				
				
				try{
					
					Reporter.log("Title of landing page : " +driver.getTitle(), true);
									
					for(int i=1; i<=TotalProductCategories.size();i++){
						try{
						String strProductCategoryName = driver.findElement(By.xpath(Constants.WebMachine_ProductsPage.ProductCategoryName+"["+i+"]"+"/h2/a")).getText();
						Reporter.log("Going to validate product in category : "+i+" : "+strProductCategoryName , false);
						fnLanguageConverter("Going to validate product in category : "+i+" : "+strProductCategoryName);
						String StrProductName = driver.findElement(By.xpath(Constants.WebMachine_ProductsPage.ProductCategoryName+"["+i+"]"+"/article/a/span")).getText();
						Reporter.log("First Product in category "+strProductCategoryName + " : "+StrProductName, false);
						fnLanguageConverter("First Product in category "+strProductCategoryName + " : "+StrProductName);
						String strProductPageUrl = driver.getCurrentUrl();
						driver.findElement(By.xpath(Constants.WebMachine_ProductsPage.ProductCategoryName+"["+i+"]"+"/article/a")).click();
						//	fnClick(ProductName_Link);
						String strNewUrl = driver.getCurrentUrl();
						boolean checkUrl = strProductPageUrl.equalsIgnoreCase(strNewUrl);
					//	System.out.println("checkUrl : "+ checkUrl);
						fnCheckCanonicalURL();
						
						try{
							
							Assert.assertTrue(fnisElementPresent(Constants.WebMachine_ProductsPage.WhereToBuy_Link), "Error! Where to buy link Not found");
							//ATUReports.add("Where to buy link is available", LogAs.PASSED, new CaptureScreen(ScreenshotOf.BROWSER_PAGE));
							Reporter.log("Where to buy link is available with link text : "+ WhereToBuy_Link.getText(), false);
							fnLanguageConverter("Where to buy link is available with link text : "+ WhereToBuy_Link.getText());
							Reporter.log("Where to buy link href : "+ WhereToBuy_Link.getAttribute("href"), false);
							fnLanguageConverter("Where to buy link href : "+ WhereToBuy_Link.getAttribute("href"));
							
						}catch(Throwable a){
							//ATUReports.add("Error! Where to buy link Not found.", LogAs.FAILED, new CaptureScreen(ScreenshotOf.BROWSER_PAGE));
							Reporter.log("Error! Where to buy link Not found for " + StrProductName , false);
							fnLanguageConverter("Error! Where to buy link Not found for " + StrProductName);
							ErrorUtil.addVerificationFailure(a);
							
						}
						if(!checkUrl){
						driver.navigate().back();
						}else{
							try{
								Reporter.log("Error ! Product "+StrProductName + " in category " + strProductCategoryName +" is Not clickable", false);
								//ATUReports.add("Error ! Product "+StrProductName + " in category " + strProductCategoryName +" is Not clickable", LogAs.FAILED, new CaptureScreen(ScreenshotOf.BROWSER_PAGE));
								fnLanguageConverter("Error ! Product "+StrProductName + " in category " + strProductCategoryName +" is Not clickable");
								Assert.fail("Error ! Product "+StrProductName + " in category " + strProductCategoryName +" is Not clickable");
							}catch(Throwable t){
								ErrorUtil.addVerificationFailure(t);
								
							}
						}
						//for(j=1;j<)
						
						}catch(Throwable b){
							Reporter.log("Error! In validating Products page.", true);
							ErrorUtil.addVerificationFailure(b);
							//driver.navigate().back();
						}
					}
					
		}catch(Throwable e){
			Reporter.log("Error! Validation of Product in category faield", true);
			ErrorUtil.addVerificationFailure(e);
			
		}
			}
		}else{
			
			Reporter.log("Error! Products landing page not found", true);
		}
		
	}catch(Throwable t){
		
		Reporter.log("Error! Validation of Product home page faield", true);
		ErrorUtil.addVerificationFailure(t);
		
	}
	
	
}
	
}


