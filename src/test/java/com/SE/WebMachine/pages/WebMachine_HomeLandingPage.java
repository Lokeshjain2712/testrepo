package com.SE.WebMachine.pages;

import java.util.ArrayList;
import java.util.Hashtable;
import java.util.List;

import org.openqa.selenium.Alert;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindAll;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.testng.Assert;
import org.testng.Reporter;

import com.SE.WebMachine.base.WebMachine_Base_Page;
import com.SE.WebMachine.util.Constants;
import com.SE.WebMachine.util.ErrorUtil;

public class WebMachine_HomeLandingPage extends WebMachine_Base_Page {

	//This will contain all the methods for validating functionality on WebMachine @Home landing page.
	
	@FindAll({@FindBy(how = How.XPATH, using = Constants.WebMachine_HomeLandingPage.HomeTipsSection_Links)})
	public List<WebElement> HomeTipsSection_Links;
	
	@FindBy(how = How.XPATH, using = Constants.WebMachine_DispatchPage.CountryName_Link)
	public WebElement CountryName_Link;
	
	@FindAll({@FindBy(how = How.XPATH, using = Constants.WebMachine_HomeLandingPage.HomeProductsSection_Links)})
	public List<WebElement> HomeProductsSection_Links;
	
	@FindAll({@FindBy(how = How.XPATH, using = Constants.WebMachine_HomeLandingPage.HomePicksSection_Links)})
	public List<WebElement> HomePicksSection_Links;
	
	@FindBy(how = How.XPATH, using = Constants.WebMachine_HomeLandingPage.EmailSignUp_Inputbox)
	public WebElement EmailSignUp_Inputbox;
	
	@FindBy(how = How.XPATH, using = Constants.WebMachine_HomeLandingPage.EmailSignUp_Button)
	public WebElement EmailSignUp_Button;
	
	@FindBy(how = How.XPATH, using = Constants.WebMachine_HomeLandingPage.ContactForm_Link)
	public WebElement ContactForm_Link;
	
	@FindBy(how = How.XPATH, using = Constants.WebMachine_HomeLandingPage.ContactFormName_Inputbox)
	public WebElement ContactFormName_Inputbox;
	
	@FindBy(how = How.XPATH, using = Constants.WebMachine_HomeLandingPage.ContactFormCompany_Inputbox)
	public WebElement ContactFormCompany_Inputbox;
	
	@FindBy(how = How.XPATH, using = Constants.WebMachine_HomeLandingPage.ContactFormAddress_Inputbox)
	public WebElement ContactFormAddress_Inputbox;
	
	@FindBy(how = How.XPATH, using = Constants.WebMachine_HomeLandingPage.ContactFormJobTitle_Inputbox)
	public WebElement ContactFormJobTitle_Inputbox;
	
	@FindBy(how = How.XPATH, using = Constants.WebMachine_HomeLandingPage.ContactFormPhone_Inputbox)
	public WebElement ContactFormPhone_Inputbox;
	
	@FindBy(how = How.XPATH, using = Constants.WebMachine_HomeLandingPage.ContactFormEmailID_Inputbox)
	public WebElement ContactFormEmailID_Inputbox;
	
	@FindBy(how = How.XPATH, using = Constants.WebMachine_HomeLandingPage.ContactFormCategory_Dropbox)
	public WebElement ContactFormCategory_Dropbox;
	
	@FindBy(how = How.XPATH, using = Constants.WebMachine_HomeLandingPage.ContactFormDescription_Inputbox)
	public WebElement ContactFormDescription_Inputbox;
	
	
	@FindBy(how = How.XPATH, using = Constants.WebMachine_HomeLandingPage.ContactFormSubmit_Button)
	public WebElement ContactFormSubmit_Button;
	
	@FindBy(how = How.XPATH, using = Constants.WebMachine_HomeLandingPage.ContactForm_Frame)
	public WebElement ContactForm_Frame;
	
	@FindBy(how = How.XPATH, using = Constants.WebMachine_HomeLandingPage.ContactFormConfirmation_check)
	public WebElement ContactFormConfirmation_check;
	
	public WebDriver driver = null;
	
	public WebMachine_HomeLandingPage(WebDriver dr){      // This is constructor of dispatch page.
		
		driver = WebMachine_Base_Page.driver;
		
	}
	
	
	
	
	public void fnValidate_ContactForm(Hashtable<String,String> data){
		
		// This will validate contact form.
		Reporter.log("Validating Contact form.", true);
		try{
		Assert.assertTrue(fnisElementPresent(Constants.WebMachine_HomeLandingPage.ContactForm_Link), "Error! Contact form Not found");
		fnClick(ContactForm_Link);
		Assert.assertTrue(fnisElementPresent(Constants.WebMachine_HomeLandingPage.ContactForm_Frame), "Error! Contact form frame Not Found");
		fnCheckCanonicalURL();
		Thread.sleep(9000);
		driver.switchTo().frame(0);
		//System.out.println(data.get("UserName"));
		//ATUReports.add("Contact form is available", LogAs.PASSED, new CaptureScreen(ScreenshotOf.BROWSER_PAGE));
		fnInputbox(ContactFormName_Inputbox, data.get("UserName"));
		//System.out.println(data.get("Company"));
		fnInputbox(ContactFormCompany_Inputbox, data.get("Company"));
		fnInputbox(ContactFormJobTitle_Inputbox, data.get("JobTitle"));
		fnInputbox(ContactFormEmailID_Inputbox, data.get("EmailAddress"));
		//fnInputbox(ContactFormEmailID_Inputbox, "xyzzz@@@@yopmai.com");
		fnInputbox(ContactFormPhone_Inputbox, data.get("Phone"));
		fnInputbox(ContactFormAddress_Inputbox, data.get("Address"));
		fnInputbox(ContactFormCategory_Dropbox, data.get("Category"));
		fnInputbox(ContactFormDescription_Inputbox, data.get("Message"));
		fnClick(ContactFormSubmit_Button);
		Assert.assertTrue(fnisElementPresent(Constants.WebMachine_HomeLandingPage.ContactFormConfirmation_check), "Error! Contact form submission failed.");
		//Assert.assertFalse(fnisElementPresent(Constants.WebMachine_HomeLandingPage.ContactFormFailMessage_check), "Error! Contact form submission completed but with failure message");
		Reporter.log("Contact form submitted successfully! with message : "+ ContactFormConfirmation_check.getText(), false);
		fnLanguageConverter("Contact form submitted successfully! with message : "+ ContactFormConfirmation_check.getText());
		//ATUReports.add("Contact form validation successfull", LogAs.PASSED, new CaptureScreen(ScreenshotOf.BROWSER_PAGE));
		driver.navigate().back();
		}catch(Throwable t){
			
			ErrorUtil.addVerificationFailure(t);
			Reporter.log("Error! Validation of Contact form failed", true);
			//ATUReports.add("Error! Validation of Contact form failed", LogAs.FAILED, new CaptureScreen(ScreenshotOf.BROWSER_PAGE));
			
		}
		
		
		
	}
	
	
	public void fnValidate_EmailSignUp(String EmailAddress){
		
		// This will validate email sign functionality on @Home landing page
		Reporter.log("Validating Email sign functionality on @Home landing page.", true);
		try{
		Assert.assertTrue(fnisElementPresent(Constants.WebMachine_HomeLandingPage.EmailSignUp_Inputbox), "Error! Email sign up Not found");
		EmailSignUp_Inputbox.sendKeys(EmailAddress);
		Assert.assertTrue(fnClick(EmailSignUp_Button),"Error! Couldn't click on EmailSignUp_Button");
		 ArrayList<String> tabNew = new ArrayList<String> (driver.getWindowHandles());
		 int intTotalhandles = tabNew.size();
		// System.out.println("handles :"+intTotalhandles);
		if(intTotalhandles!=1){
		fnSwitchToNewWindow();
		Assert.assertTrue(fnisElementPresent(Constants.WebMachine_HomeLandingPage.EmailSubscriptionConfirmation_check), "Error! Email subscription failed for email address : " + EmailAddress);
		Reporter.log("Email subscription successfull for email address : " + EmailAddress, true);
		//ATUReports.add("Email subscription successfull", LogAs.PASSED, new CaptureScreen(ScreenshotOf.BROWSER_PAGE));
		driver.close();
		fnSwitchtoMainwindow();
		}else{
			
			Alert alert = driver.switchTo().alert();
			String strAlertText = alert.getText();
			alert.accept();
			Reporter.log("Alert says : " +strAlertText, false);
			fnLanguageConverter("Alert says : " +strAlertText);
			Assert.fail("Error! Email signup failed due to wrong email address");
		}
		
		}catch(Throwable t){
			
			ErrorUtil.addVerificationFailure(t);
			Reporter.log("Error! Validation failed for email signup functionality", true);
			//ATUReports.add("Error!Validation failed for email signup functionality", LogAs.FAILED, new CaptureScreen(ScreenshotOf.BROWSER_PAGE));
		}
		
	}
	
	public void fnValidate_HomeLandingPageContent(){
		
		// This will validate content of @Home landing page.
		
		try{
			
			Assert.assertTrue(HomeTipsSection_Links.size()!=0, "Error! Home tips section Not found.");
			Reporter.log("For Country : "+CountryName_Link.getText() +", Tips section is available on @Home landing page with total sub sections : "+ HomeTipsSection_Links.size(), true);
			for(int i=1;i<=HomeTipsSection_Links.size();i++){
				try{
					
					String strTipName = driver.findElement(By.xpath(Constants.WebMachine_HomeLandingPage.HomeTipsSection_Links+"["+i+"]//h3")).getText();
				//	driver.findElement(By.xpath("//ul[@class='tips-section']/li//a")).click();
					Reporter.log("Home Tip "+i+" : "+strTipName , false);
					fnLanguageConverter("Home Tip "+i+" : "+strTipName );
					
								
				}catch(Throwable e){
					
					ErrorUtil.addVerificationFailure(e);
				}
				
			}
			
			// Now validate the tips section.
			
			for(int i=1;i<=HomeTipsSection_Links.size();i++){
				try{
					String strTipName = driver.findElement(By.xpath(Constants.WebMachine_HomeLandingPage.HomeTipsSection_Links+"["+i+"]//h3")).getText();
					Reporter.log("Validating @Home tip "+i+" : "+strTipName, false);
					fnLanguageConverter("Validating @Home tip "+i+" : "+strTipName);
					try{
					driver.findElement(By.xpath(Constants.WebMachine_HomeLandingPage.HomeTipsSection_Links+"["+i+"]//a")).click();
					fnCheckCanonicalURL();
					}catch(Throwable t){
						Reporter.log("Error! "+strTipName+" not clickable", false);
						fnLanguageConverter("Error! "+strTipName+" not clickable");
						Assert.fail("Error! "+strTipName+" not clickable");
						ErrorUtil.addVerificationFailure(t);
						
					}
					try{
					Assert.assertTrue(fnValidateTitle(), "Home Tip "+i+" : "+strTipName + " Not Working");
					Reporter.log("Home Tip "+i+" : "+strTipName + " Working fine" , false);
					fnLanguageConverter("Home Tip "+i+" : "+strTipName + " Working fine");
					driver.navigate().back();
					}catch(Throwable e){
						driver.navigate().back();
						Reporter.log("Error! in validating Home Tip "+i , true);
						ErrorUtil.addVerificationFailure(e);
					}
								
				}catch(Throwable e){
					Reporter.log("Error! in validating Home Tip "+i , true);
					ErrorUtil.addVerificationFailure(e);
				}
				
			}
				
			
			
			
		}catch(Throwable t){
			
			ErrorUtil.addVerificationFailure(t);
			Reporter.log("Error! Home tips section Not found.", true);
		}
		
		
		// Now validate products section
		
		try{
			
			Assert.assertTrue(HomeProductsSection_Links.size()!=0, "Error! Home Products section Not found.");
			String strProductsTitle = driver.findElement(By.xpath(Constants.WebMachine_HomeLandingPage.HomeProductsSection_Links+"/h2")).getText();
			Reporter.log("For Country : "+CountryName_Link.getText() +", Home products range is available with title : "+ strProductsTitle, true);
			List<WebElement> eleProducts = driver.findElements(By.xpath(Constants.WebMachine_HomeLandingPage.HomeProductsSection_Links+"//ul/li"));
			int intTotalProducts = eleProducts.size();
			Reporter.log("Total Home products found : "+ intTotalProducts, true);
			Assert.assertTrue(intTotalProducts!=0, "Error! No Home products found");
			Reporter.log("List of 5 Home products visible ", true);
			for(int i=1;i<=5;i++){
				try{
					
					String strHomeProductName = driver.findElement(By.xpath(Constants.WebMachine_HomeLandingPage.HomeProductsSection_Links+"//ul/li["+i+"]/a//span")).getText();
				
					Reporter.log("Home product name "+i+" : "+strHomeProductName , false);
					fnLanguageConverter("Home product name "+i+" : "+strHomeProductName);
					
								
				}catch(Throwable e){
					Reporter.log("Error! Home product "+i+" Not found",true);
					ErrorUtil.addVerificationFailure(e);
				}
				
			}
			
			// Now click on product options available and validate their landing page.
			
			for(int i=1;i<=5;i++){
				try{
					String strHomeProductName = driver.findElement(By.xpath(Constants.WebMachine_HomeLandingPage.HomeProductsSection_Links+"//ul/li["+i+"]/a//span")).getText();
					
					Reporter.log("Validating Home product "+i+" : "+strHomeProductName , false);
					fnLanguageConverter("Validating Home product "+i+" : "+strHomeProductName);
					try{
						driver.findElement(By.xpath(Constants.WebMachine_HomeLandingPage.HomeProductsSection_Links+"//ul/li["+i+"]/a")).click();
						fnCheckCanonicalURL();
					}catch(Throwable t){
						Reporter.log("Error! "+strHomeProductName+" not clickable", false);
						fnLanguageConverter("Error! "+strHomeProductName+" not clickable");
						Assert.fail("Error! "+strHomeProductName+" not clickable");
						ErrorUtil.addVerificationFailure(t);
						
					}
					try{
					Assert.assertTrue(fnValidateTitle(), " Home product "+i+" : "+strHomeProductName + " Not Working");
					Reporter.log("Home product "+i+" : "+strHomeProductName + " Working fine" , false);
					fnLanguageConverter("Home product "+i+" : "+strHomeProductName + " Working fine");
					driver.navigate().back();
					}catch(Throwable e){
						driver.navigate().back();
						Reporter.log("Error! in validating Home product "+i , true);
						ErrorUtil.addVerificationFailure(e);
					}
								
				}catch(Throwable e){
					Reporter.log("Error! in validating Home product "+i , true);
					ErrorUtil.addVerificationFailure(e);
				}
				
			}
			
			
		}catch(Throwable t){
			
			Reporter.log("Error! in Validating Home products section ", true);
			ErrorUtil.addVerificationFailure(t);
			
		}
		
	
		// Now validate Today's picks section on @Home landing page.
		
		try{
			
			Assert.assertTrue(HomePicksSection_Links.size()!=0, "Error! Home Picks section Not found.");
			String strTodaysPicksTitle = driver.findElement(By.xpath(Constants.WebMachine_HomeLandingPage.HomePicksSection_Links+"/h2")).getText();
			Reporter.log("For Country : "+CountryName_Link.getText() +", Home today's picks section is available with title : "+ strTodaysPicksTitle, false);
			fnLanguageConverter("For Country : "+CountryName_Link.getText() +", Home today's picks section is available with title : "+ strTodaysPicksTitle);
			List<WebElement> elePicksTiles = driver.findElements(By.xpath(Constants.WebMachine_HomeLandingPage.HomePicksSection_Links+"//ul[@class='tiles']/li"));
			int intTotalPicksTiles = elePicksTiles.size();
			int intLargerTiles = 0;
			int intSmallerTiles = 0;
			for(int i=1;i<=intTotalPicksTiles;i++){
				
			String strTileType = driver.findElement(By.xpath(Constants.WebMachine_HomeLandingPage.HomePicksSection_Links+"//ul[@class='tiles']/li["+i+"]")).getAttribute("class");
				if(strTileType.equalsIgnoreCase("larger")){
					intLargerTiles++;
				}else{
					
					intSmallerTiles++;
				}
			
			}
			Reporter.log("Number of larger tiles in Picks section : "+ intLargerTiles, true);
			Reporter.log("Number of smaller tiles in Picks section : "+ intSmallerTiles, true);
			
		}catch(Throwable t){
			
			Reporter.log("Error! in Validating Home Picks section ", true);
			ErrorUtil.addVerificationFailure(t);
			
		}
		
		
	}
	
	
	
}
